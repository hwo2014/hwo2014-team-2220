# -*- coding: utf-8 -*-

from towers import *
from cars import *
from util import in_private
from util.benchmark import Benchmark
import socket

def constant_car_0_65():
    return CompiledCar(by_tick={0:0.65})

def save_the_bacon_car():
    """7.70s solution from SaveTheBacon@reddit."""
    items = [
        (1.0, [33, 34, 35, 36, 37, 38, 39, 0, 1, 2]),
        (0.0, [3]),
        (0.6, [4]),
        (0.7, [5, 6]),
        (1.0, [7, 8, 9, 10, 11]),
        (0.0, [12]),
        (0.6, [13, 14, 15, 16, 17]),
        (0.75, [18, 19, 20, 21]),
        (1.0, [22, 23, 24]),
        (0.0, [25]),
        (0.7, [26, 27, 28, 29, 30, 31]),
        (0.5, [32]),
    ]
    by_track = dict()
    for throttle, tracks in items:
        for track in tracks:
            by_track[track] = throttle
    return CompiledCar(by_track=by_track, name="save_the_bacon")

def be_the_good_girl(host, port, name, key):
    """Offical submission."""
    try:
        print("host={0}, port={1}, name={2}, key=*".format(host, port, name, key))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))

        verbose = in_private()
        car = constant_car_0_65()
        car.name = name
        tower = ControlTower(s, key, car, verbose=verbose)
        tower.run()
    except Exception as e:
        print("Exception({0}): {1}".format(e.errno, e.strerror))

def go_play_outside_with_compiled_car(host, port, name, key):
    """Catch some idea on physics!"""
    # print("host={0}, port={1}, name={2}, key=*".format(host, port, name, key))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, int(port)))

    car = constant_car_0_65()
    verbose = True
    tower = ControlTower(s, key, car, verbose=verbose)
    tower.run()

def go_play_outside_with_top_speed_car(host, port, name, key):
    """Don't bother the server."""
    keimola = "5ba295bb-f343-4ee6-b45a-92b3ad85bbec"
    germany = "1f5238db-93f9-4920-a994-75d929e627cf"
    U_S_A = "e7df0508-2dfd-4810-a5c7-81c6ba4255e8"
    save_the_bacon = "240809da-876f-4424-b247-dc815f60f4ac"
    game_id = keimola
    car = Benchmark(game_id)()
    # game_id = save_the_bacon
    # car = save_the_bacon_car()

    verbose = False
    tower = MnesiaTower(game_id, key, car, verbose=verbose)
    tower.run()

def go_play_outside_with_random_cars(host, port, name, key):
    """Don't bother the server."""
    game_ids = """
5ba295bb-f343-4ee6-b45a-92b3ad85bbec
1f5238db-93f9-4920-a994-75d929e627cf
e7df0508-2dfd-4810-a5c7-81c6ba4255e8
240809da-876f-4424-b247-dc815f60f4ac
a66e4a04-76b6-412c-a2ea-eb3dc67b8c39
1db8ca1f-be58-4401-a59d-17ac9a7bf506
30f3ff93-908e-408c-9c7d-63caec27ec18
6bf8cbab-b1c5-4efa-aa30-8d505049c9ca
ff0b255e-d502-432a-a7cf-35bc34cebbd8
63184e5c-5b0a-43c7-95cd-06f7cd101b49
c00a737b-8cd0-450b-b7d0-f7500fdec71d
b1f35204-e44d-491c-8d69-37d3786aa38e
1d028ffc-097c-419a-b280-37d137443ad9
b2e35e12-8c62-4114-a664-6dafe851085e
2920f019-3a4f-45ae-baaf-1641eeba5e88
e6679d64-65d9-4e1c-979d-ad452bb8e3e3
633e19f6-b382-488f-901c-f880dbc32247
036aa1cc-13d1-44fd-b51b-8a906e7185fe
aaf41d5f-0434-4261-8ad4-08c664fe6162
8cc35ba2-70f0-4781-9372-46451d545dd3
cfcf7ea1-06ef-45d3-882f-336f9a1a462a
0aec3a55-d6f5-4381-95cc-2255b10fe9b2
8cac47c2-cafd-4005-8c51-145cd69c01d8
""".strip().splitlines()

    game_ids = ["e90e6136-a0cf-4a3f-bd02-6d908997c7ff"]  # 0.60

    for game_id in game_ids:
        car = InspectCar()
        verbose = False
        tower = MnesiaTower(game_id, key, car, verbose=verbose)
        tower.run()

if __name__ == "__main__":
    import sys

    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
        sys.exit()

    host, port, name, key = sys.argv[1:5]
    if in_private():
        # go_play_outside_with_reinforced_car(host, port, name, key)
        # go_play_outside_with_compiled_car(host, port, name, key)
        go_play_outside_with_top_speed_car(host, port, name, key)
        # go_play_outside_with_random_cars(host, port, name, key)
    else:
        be_the_good_girl(host, port, name, key)
