class TeamMoment(object):
    """
    Temporal data for specific team.

    See cls.next_moment for inherited variables.
    """

    def __init__(self):
        # car_positions: state-specific terms.
        self.game_tick = None
        self.lap_count = None

        self.track_piece_id = None
        self.track_piece = None  # cached sugar.

        self.begin_lane_id = None
        self.end_lane_id = None
        self.lane = None  # cached sugar.

        self.in_piece_distance = None
        self.in_piece_angle = None

        # Supportive but not state-specific term.
        self.observed_throttle = None  # may be activated at past data.

        # on_game_init: inherited values.
        self.disqualified = None
        self.crashed = None
        self.finished = None
        self.turbo_available = None
        self.turbo_ticks_left = None  # 0: finished

        # GameState.this_is_initial_state, advance: continuous values.
        self.travel_distance = None
        self.velocity = None
        self.acceleration = None
        self.jerk = None

        self.travel_angle = None
        self.angular_velocity = None
        self.angular_acceleration = None
        self.angular_jerk = None

        self.in_piece_angular_velocity = None
        self.in_piece_angular_acceleration = None
        self.in_piece_angular_jerk = None

        # GameState.fill_estimations, this_is_initial_state: estimations are in the future space.
        self.acceleration_hat = None
        self.velocity_hat = None
        self.travel_distance_hat = None

        # Semi-const terms from the server.
        self.turbo_duration_time = None
        self.turbo_duration_ticks = None
        self.turbo_factor = None

        # Messages to the server: changing these values results in communicating to server at last.
        self.throttle_next = None  # inherited
        self.switch_lane_next = None  # case sensitive: Left, Right. Switch on the next available track.
        self.turbo_next = None

    def next_moment(self):
        future = TeamMoment()
        present = self

        # Inheritance from the present.
        future.throttle_next = present.throttle_next

        return future
