from collections import defaultdict
from .teamstate import TeamState
from .trackstate import TrackState
from util import floats

class GameState(object):
    """Overall game state."""

    def __init__(self, history_length):
        self.history_length = history_length

        # Have something to send to?
        self.need_reply_to_the_server = None

        # State per team.
        self._team_states = defaultdict(TeamState)

        # Semi-const values from the server.
        # laps: Test=3, Qualifying=None, Race=5
        self.max_lap_count = None
        # Test:60000, Qualifying:None, Race:60000
        self.max_lap_time = None
        # quick_race: Test=True, Qualifying=None, Race=False
        self.quick_race = None

        # Message from the server.
        self.our_team_color = None
        self.our_team_name = None
        self.game_id = None

        self.joined = None
        self.joined_key = None
        self.joined_team_name = None

        self.track_state = TrackState()

        # Starting information.
        self.our_team_starting_position = None
        self.our_team_starting_angle = None

    def __getitem__(self, key):
        """Team color is the key."""
        if key is None:
            key = self.our_team_color
        return self._team_states[key]

    def this_is_initial_state(self):
        for team_state in self._team_states.itervalues():
            present = team_state[0]
            present.travel_distance = 0
            present.velocity = 0
            present.acceleration = 0
            present.jerk = 0

            present.track_piece_hat = present.track_piece
            present.lane_hat = present.lane

            present.travel_angle = 0
            present.angular_velocity = 0
            present.angular_acceleration = 0
            present.angular_jerk = 0

            present.in_piece_angular_velocity = 0
            present.in_piece_angular_acceleration = 0
            present.in_piece_angular_jerk = 0

            present.jerk_hat = 0
            present.acceleration_hat = 0
            present.velocity_hat = 0
            present.travel_distance_hat = 0

            present.angular_jerk_hat = 0
            present.angular_acceleration_hat = 0
            present.angular_velocity_hat = 0
            present.travel_angle_hat = 0

            present.in_piece_angular_jerk_hat = 0
            present.in_piece_angular_acceleration_hat = 0
            present.in_piece_angular_velocity_hat = 0
            present.in_piece_angle_hat = 0

            team_state.this_is_initial_state(self.history_length)


    def advance(self):
        for team_state in self._team_states.itervalues():
            team_state.advance(self.history_length)

            future = team_state[0]
            present = team_state[1]
            past = team_state[2]

            present.travel_distance = past.travel_distance + present.in_piece_distance - past.in_piece_distance
            # Do not use track_state.net_displacement here: lane switch may result in some different result.
            if present.track_piece_id != past.track_piece_id:
                present.travel_distance += past.lane.length()
            present.velocity = present.travel_distance - past.travel_distance
            present.acceleration = present.velocity - past.velocity
            present.jerk = present.acceleration - past.acceleration

            #present.travel_angle = self.track_state.net_angular_displacement * past.lap_count + past.lane.in_piece_distance_to_piece_angle(past.in_piece_distance)
            present.travel_angle = self.track_state.net_angular_displacement * present.lap_count + present.lane.in_piece_distance_to_piece_angle(present.in_piece_distance)
            #present.travel_angle += present.in_piece_angle
            present.angular_velocity = present.travel_angle - past.travel_angle
            present.angular_acceleration = present.angular_velocity - past.angular_velocity
            present.angular_jerk = present.angular_acceleration - past.angular_acceleration

            present.in_piece_angular_velocity = present.in_piece_angle - past.in_piece_angle
            present.in_piece_angular_acceleration = present.in_piece_angular_velocity - past.in_piece_angular_velocity
            present.in_piece_angular_jerk = present.in_piece_angular_acceleration - past.in_piece_angular_acceleration

    def fill_estimations(self):
        for team_state in self._team_states.itervalues():
            # We are the future!
            future = team_state[0]
            present = team_state[1]
            past = team_state[2]
            ppast = team_state[3]
            pppast = team_state[3]

            # Positional estimation
            # 7.27595761418e-12 error -3.63797880709e-12,3.63797880709e-12
            # BEST!
            future.acceleration_hat = 0.2 * present.throttle_next - 0.02 * present.velocity

            future.jerk_hat = future.acceleration_hat - present.acceleration
            # future.acceleration_hat = 0
            future.velocity_hat = present.velocity + future.acceleration_hat
            future.travel_distance_hat = present.travel_distance + future.velocity_hat

            # Track change
            f_in_piece_distance = present.in_piece_distance + future.velocity_hat
            if f_in_piece_distance > present.lane.length():
                f_track_piece_id = (present.track_piece_id + 1) % len(self.track_state.track_pieces)
                future.lap_count_hat = present.lap_count + 1
            else:
                f_track_piece_id = present.track_piece_id
                future.lap_count_hat = present.lap_count
            future.track_piece_hat = self.track_state[f_track_piece_id]
            future.lane_hat = future.track_piece_hat[present.end_lane_id, present.end_lane_id]  # lane end, so it's (end, end), not (begin, end).

            p = present

            future.angular_jerk_hat = 0
            future.angular_acceleration_hat = present.angular_acceleration + future.angular_jerk_hat
            future.angular_velocity_hat = present.angular_velocity + future.angular_acceleration_hat
            future.travel_angle_hat = present.travel_angle + future.angular_velocity_hat

            # # 0.0108120528377 error -0.00425627280276,0.00655578003494
            # # 0.61948 point.
            _cf = p.track_piece.inversed_signed_radius * (p.velocity ** 2)
            _A = p.in_piece_angle * p.velocity
            _B = p.in_piece_angular_velocity * p.velocity
            _C = p.in_piece_angular_acceleration * p.velocity
            _D = p.in_piece_angular_jerk * p.velocity
            # future.in_piece_angular_acceleration_hat = -4.882126497410418e-05
            # future.in_piece_angular_acceleration_hat += -0.010985890777455327 * present.in_piece_angle
            # future.in_piece_angular_acceleration_hat += -0.10740962306172304 * present.in_piece_angular_velocity
            # future.in_piece_angular_acceleration_hat += 0.018826216233673955 * _cf
            # future.in_piece_angular_acceleration_hat += 7.276483773684708e-05 * _A

            # 0.00874367201779 error -0.00427756685294,0.00446610516485
            # 0.5009755 points. BEST SO FAR.
            _cfH = p.track_piece.inversed_signed_radius * (future.velocity_hat ** 2)
            _AH = p.in_piece_angle * future.velocity_hat
            _BH = p.in_piece_angular_velocity * future.velocity_hat
            _CH = p.in_piece_angular_acceleration * future.velocity_hat
            _DH = p.in_piece_angular_jerk * future.velocity_hat
            future.in_piece_angular_acceleration_hat = -0.0006519335182300445
            future.in_piece_angular_acceleration_hat += -0.0016786631068959792 * present.in_piece_angle
            future.in_piece_angular_acceleration_hat += -0.08734083770890541 * present.in_piece_angular_velocity
            future.in_piece_angular_acceleration_hat += 0.012729493510880659 * _cfH
            future.in_piece_angular_acceleration_hat += -0.0008831131583053715 * _AH

            _cfL = p.lane.inversed_signed_radius * (present.velocity ** 2)
            _cfLH = p.lane.inversed_signed_radius * (future.velocity_hat ** 2)
            _m_cfH = (lambda cut:(max(abs(_cfH), cut)-cut) * cmp(_cfH, 0))(0.29528993413624594)

            # 0.00341184366514 error -0.0012196298437,0.00219221382144
            # 0.195484 points. BEST SO FAR.
            future.in_piece_angular_acceleration_hat = -0.0008897497714358326
            future.in_piece_angular_acceleration_hat += -0.0003713190242523886 * present.in_piece_angle
            future.in_piece_angular_acceleration_hat += -0.09783015355817692 * present.in_piece_angular_velocity
            future.in_piece_angular_acceleration_hat += 0.039425779180993 * _m_cfH
            future.in_piece_angular_acceleration_hat += -0.0011407201527727179 * _AH

            # # 0.00512046184954 error -0.00126939420848,0.00385106764106
            # future.in_piece_angular_acceleration_hat = 0.0
            # future.in_piece_angular_acceleration_hat += 0.0063824376742064644 * present.in_piece_angle
            # future.in_piece_angular_acceleration_hat += -0.09047875906123885 * present.in_piece_angular_velocity
            # future.in_piece_angular_acceleration_hat += 0.03527788705218809 * _m_cfH
            # future.in_piece_angular_acceleration_hat += -0.00204741072652501 * _AH

            # custom
            _m_cfH = (lambda cut:(max(abs(_cfH), cut)-cut) * cmp(_cfH, 0))(0.3)
            future.in_piece_angular_acceleration_hat = 0.0
            future.in_piece_angular_acceleration_hat += 0.006 * present.in_piece_angle
            future.in_piece_angular_acceleration_hat += -0.01 * present.in_piece_angular_velocity
            future.in_piece_angular_acceleration_hat += 0.01 * _m_cfH
            future.in_piece_angular_acceleration_hat += -0.001 * _AH

            # #future.in_piece_angular_jerk_hat = 0
            # #future.in_piece_angular_acceleration_hat = present.in_piece_angular_acceleration + future.in_piece_angular_jerk_hat
            # future.in_piece_angular_acceleration_hat = 0
            future.in_piece_angular_velocity_hat = present.in_piece_angular_velocity + future.in_piece_angular_acceleration_hat
            future.in_piece_angle_hat = present.in_piece_angle + future.in_piece_angular_velocity_hat

            # # future.in_piece_angle_hat = 0
            # future.in_piece_angular_velocity_hat = future.in_piece_angle_hat - present.in_piece_angle
            # future.in_piece_angular_acceleration_hat = future.in_piece_angular_velocity_hat - present.in_piece_angular_velocity
            # future.in_piece_angular_jerk_hat = future.in_piece_angular_acceleration_hat - present.in_piece_angular_jerk

