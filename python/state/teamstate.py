from collections import deque
from .teammoment import TeamMoment
import copy

class TeamState(object):
    """Team state data."""

    def __init__(self):
        # Temporal data for the team.
        self._moments = deque()
        self.advance()

        # Identity.
        self.color = None
        self.name = None
        self.car_width = None
        self.car_length = None
        self.car_guide_flag_position = None

        # etc.
        self.last_throttle = 0

    def __getitem__(self, key):
        """
        [0] means the future state that we should estimate and build up.
        [1] means the latest state from the server that would be never changed.
        [2] second-old one from the server.

        ...
        """
        return self._moments[key]

    def this_is_initial_state(self, history_length):
        for i in xrange(history_length):
            if self._moments:
                moment = copy.copy(self._moments[0])
            else:
                moment = TeamMoment()

            self._moments.appendleft(moment)
            if len(self._moments) > history_length+1:
                self._moments.pop()

    def advance(self, history_length=None):
        if self._moments:
            moment = self._moments[0].next_moment()
        else:
            moment = TeamMoment()

        self._moments.appendleft(moment)
        if history_length and len(self._moments) > history_length+1:
            self._moments.pop()

    def clear(self):
        self._moments.clear()
