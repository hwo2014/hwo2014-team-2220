from .lanepiece import *
from itertools import chain, izip
import math
import cmath

class TrackPiece(object):
    def __init__(self, track_piece_id,
                 length, switch, central_angle, radius,
                 begin_position, begin_angle,
                 lane_distances_from_center):
        self.track_piece_id = track_piece_id
        type_ = None
        if length is None:
            type_ = "c"  # corner
            length = 0

        if switch is None:
            switch = False

        if central_angle is None:
            type_ = "s"  # straight
            central_angle = 0

        if radius is None:
            type_ = "s"  # straight
            radius = 0

        if type_ is None:
            type_ = "s"  # straight

        self.type_ = type_
        self.length = length
        self.switch = switch
        self.central_angle = central_angle
        self.signed_radius = radius * cmp(self.central_angle, 0)
        self.inversed_signed_radius = 1.0/self.signed_radius if self.signed_radius != 0 else 0

        self.begin_position = begin_position
        self.begin_angle = begin_angle
        self.net_displacement = None
        self.net_angular_displacement = None

        self.lane_distance_map = lane_distances_from_center
        self.lane_pieces = dict()
        self._update_track_piece()
        self._update_lane_pieces()

    @property
    def end_position(self):
        return self.begin_position + self.net_displacement

    @property
    def end_angle(self):
        return self.begin_angle + self.net_angular_displacement

    def __getitem__(self, key):
        return self.lane_pieces[key]

    def _update_track_piece(self):
        if self.type_ == "s":
            self.net_displacement = self.length
        else:
            # assert self.type_ == "c".
            length = 2 * self.signed_radius * math.sin(self.central_angle/2)  # radius(-) * sin(-) = length(+) >= 0 always.
            self.net_displacement = cmath.rect(length, self.central_angle/2 + self.begin_angle)
        self.net_angular_displacement = self.central_angle

    def _update_lane_pieces(self):
        lane_ids = list(self.lane_distance_map.keys())
        if self.switch:
            # Generate all (non-)switching lanes.
            lane_pairs = chain(izip(lane_ids, lane_ids), izip(lane_ids[1:], lane_ids[:-1]), izip(lane_ids[:-1], lane_ids[1:]))
        else:
            # Non-switching lanes only.
            lane_pairs = izip(lane_ids, lane_ids)

        for begin_id, end_id in lane_pairs:
            self._update_lane_piece(begin_id, end_id)

    def _update_lane_piece(self, begin_lane_id, end_lane_id):
        begin_offset = self.lane_distance_map[begin_lane_id]
        end_offset = self.lane_distance_map[end_lane_id]
        offset = begin_offset if begin_lane_id == end_lane_id else None

        if self.type_ == "s":
            if begin_lane_id == end_lane_id:
                lane = StraightLanePiece(
                    length=self.length,
                    offset=offset,
                    begin_position=self.begin_position, begin_angle=self.begin_angle)
            else:
                lane = StraightSwitchPiece(
                    length=self.length,
                    begin_offset=begin_offset, end_offset=end_offset,
                    begin_position=self.begin_position, begin_angle=self.begin_angle)
        else:
            # assert self.type_ == "a"
            if begin_lane_id == end_lane_id:
                lane = CurvedLanePiece(
                    signed_radius=self.signed_radius, central_angle=self.central_angle,
                    offset=offset,
                    begin_position=self.begin_position, begin_angle=self.begin_angle)
            else:
                lane = CurvedSwitchPiece(
                    signed_radius=self.signed_radius, central_angle=self.central_angle,
                    begin_offset=begin_offset, end_offset=end_offset,
                    begin_position=self.begin_position, begin_angle=self.begin_angle)

        self.lane_pieces[(begin_lane_id, end_lane_id)] = lane
