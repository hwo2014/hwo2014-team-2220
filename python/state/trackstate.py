from .trackpiece import TrackPiece
from collections import defaultdict
import math

class TrackState(object):
    def __init__(self):
        self.track_id = None
        self.name = None
        self.track_pieces = []
        self.lane_distances_from_center = defaultdict(float)
        self.begin_position = complex(0, 0)
        self.begin_angle = 0
        self.net_displacement = complex(0, 0)
        self.net_angular_displacement = 0

    @property
    def end_position(self):
        return self.begin_position + self.net_displacement

    @property
    def end_angle(self):
        return self.begin_angle + self.net_angular_displacement

    def __getitem__(self, key):
        _key = key % len(self.track_pieces)
        return self.track_pieces[_key]

    def update_lanes(self, lanes):
        self.lane_distances_from_center.update(lanes)

    def append(self, **kw):
        track_piece = TrackPiece(
            track_piece_id=len(self.track_pieces),
            lane_distances_from_center=self.lane_distances_from_center,
            begin_position=self.end_position,
            begin_angle=self.end_angle,
            **kw)
        self.track_pieces.append(track_piece)

        self.net_displacement += track_piece.net_displacement
        self.net_angular_displacement += track_piece.net_angular_displacement
