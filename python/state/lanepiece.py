import abc
import cmath
import math
from geometry import *

class LanePiece(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self, begin_position, begin_angle):
        self.begin_position = begin_position
        self.begin_angle = begin_angle

        self.translation = begin_position
        self.rotation = cmath.rect(1, begin_angle)

    @abc.abstractmethod
    def length(self):
        pass

    @abc.abstractmethod
    def piece_angle(self, t):
        pass

    def in_piece_distance_to_piece_angle(self, in_piece_distance):
        t = in_piece_distance / self.length()
        return self.piece_angle(t)

    def piece_angle(self, t):
        d = self.curve.derivative(t) / self.rotation
        theta = cmath.phase(d) + self.begin_angle
        return theta

    @property
    def inversed_signed_radius(self):
        return 0 if self.signed_radius == 0 else 1/self.signed_radius
            
class StraightLanePiece(LanePiece):
    def __init__(self, length, offset, begin_position, begin_angle):
        super(StraightLanePiece, self).__init__(begin_position=begin_position, begin_angle=begin_angle)
        a = complex(0, offset) * self.rotation + self.translation
        b = complex(length, offset) * self.rotation + self.translation
        curve = LinearBezier(a, b)

        self.signed_radius = 0
        self.curve = curve
        self._length = curve.length()

    def length(self):
        return self._length

class StraightSwitchPiece(LanePiece):
    def __init__(self, length, begin_offset, end_offset, begin_position, begin_angle):
        super(StraightSwitchPiece, self).__init__(begin_position=begin_position, begin_angle=begin_angle)
        a = complex(0, begin_offset) * self.rotation + self.translation
        b = complex(length/2, begin_offset) * self.rotation + self.translation
        c = complex(length/2, end_offset) * self.rotation + self.translation
        d = complex(length, end_offset) * self.rotation + self.translation
        curve = CubicBezier(a, b, c, d)

        self.signed_radius = 0  # XXX
        self.curve = curve
        self._length = curve.length()

    def length(self):
        return self._length

class CurvedLanePiece(LanePiece):
    def __init__(self, signed_radius, central_angle, offset, begin_position, begin_angle):
        super(CurvedLanePiece, self).__init__(begin_position=begin_position, begin_angle=begin_angle)
        center = complex(0, signed_radius) * self.rotation + self.translation
        # clockwise(+) with right(+) = reduce_radius(-)
        _signed_radius = signed_radius - offset
        _begin_angle = (-0.5) * math.pi + self.begin_angle
        angular_displacement = central_angle

        curve = Arc(center, _signed_radius, _begin_angle, angular_displacement)

        self.signed_radius = _signed_radius
        self.curve = curve
        self._length = curve.length()

    def length(self):
        return self._length

class CurvedSwitchPiece(LanePiece):
    def __init__(self, signed_radius, central_angle, begin_offset, end_offset, begin_position, begin_angle):
        super(CurvedSwitchPiece, self).__init__(begin_position=begin_position, begin_angle=begin_angle)
        start_signed_radius = signed_radius - begin_offset
        end_signed_radius = signed_radius - end_offset
        mid_signed_radius = (start_signed_radius + end_signed_radius) / 2.0

        a = cmath.rect(start_signed_radius, 0) * self.rotation + self.translation
        b = cmath.rect(mid_signed_radius, central_angle/2.0) * self.rotation + self.translation
        c = cmath.rect(end_signed_radius, central_angle) * self.rotation + self.translation

        curve = QuadraticBezier(a, b, c)
        self.signed_radius = start_signed_radius  # XXX
        self.curve = curve
        self._length = curve.length()
        self._central_angle = central_angle
    
    def length(self):
        return self._length

