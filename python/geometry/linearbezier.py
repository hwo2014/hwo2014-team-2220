class LinearBezier(object):
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def __call__(self, t):
        p = self.a * (1-t) + self.b * t
        return p

    def derivative(self, t):
        d_self_t = self.b - self.a
        return d_self_t

    def length(self):
        l = abs(self.b - self.a)
        return l
