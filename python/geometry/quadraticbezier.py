from .linearbezier import LinearBezier

class QuadraticBezier(object):
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c
        self.ab = LinearBezier(self.a, self.b)
        self.bc = LinearBezier(self.b, self.c)

    def __call__(self, t):
        p = self.ab(t) * (1-t) + self.bc(t) * t
        return p

    def derivative(self, t):
        d_self_t = self.ab.derivative(t) * (1-t) - self.ab(t) + self.bc.derivative(t) * t + self.bc(t)
        return d_self_t

    def length(self):
        n_slot = 10

        l = 0
        u = self(0)
        for i in xrange(1, n_slot+1):
            t = float(i) / n_slot
            v = self(t)
            l += abs(v - u)
            u = v

        # 1.006 +
        # 1.0058 (+)
        # 1.0057 (+)
        # 1.00560 (+)
        # 1.0055500 ((+))
        # 1.0055400 ((((+))))
        # 1.0055390 (((((+)))))
        # 1.0055385 (((((+)))))
        # 1.0055380 (((((??)))))
        # 1.0055370 (((((??)))))
        r=1.0055350  # seems perfect
        # 1.0055325 ((((-))))
        # 1.0055250 (((-)))
        # 1.005500 ((-))
        # 1.004 --
        return l * r
