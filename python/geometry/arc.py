import cmath

class Arc(object):
    def __init__(self, center, signed_radius, begin_angle, angular_displacement):
        self.c = center
        self.r = signed_radius
        self.theta_zero = begin_angle
        self.delta_theta = angular_displacement

    def __call__(self, t):
        theta = self.theta_zero + self.delta_theta * t
        p = self.c + cmath.rect(self.r, theta)
        return p

    def derivative(self, t):
        theta = self.theta_zero + self.delta_theta * t
        d_theta = self.delta_theta
        d_self_t = cmath.rect(self.r, theta + cmath.pi/2*cmp(self.delta_theta,0)) * d_theta
        return d_self_t

    def length(self):
        # radius(-) * delta_theta(-) = length(+)
        return self.r * self.delta_theta
