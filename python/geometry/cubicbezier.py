from .quadraticbezier import QuadraticBezier

class CubicBezier(object):
    def __init__(self, a, b, c, d):
        self.a = a
        self.b = b
        self.c = c
        self.d = d
        self.abc = QuadraticBezier(a, b, c)
        self.bcd = QuadraticBezier(b, c, d)

    def __call__(self, t):
        p = self.abc(t) * (1-t) + self.bcd(t) * t
        return p

    def derivative(self, t):
        d_self_t = self.abc.derivative(t) * (1-t) - self.abc(t) + self.bcd.derivative(t) * t + self.bcd(t)
        return d_self_t

    def length(self):
        n_sample = 100

        l = 0
        u = self(0)
        for i in xrange(1, n_sample+1):
            t = float(i) / n_sample
            v = self(t)
            l += abs(v - u)
            u = v

        # 1.0 +
        # 0.995 ((+))
        # 0.99375 ((+))
        # 0.993200 (((+)))
        # 0.993170 (((+)))
        # 0.993125 (((+)))
        # 0.993060 ((((+))))
        # 0.993050 (((((+)))))
        r=0.993045  # seems perfect
        # 0.993040 (((((-)))))
        # 0.993000 (((-)))
        # 0.9925 ((-))
        # 0.99 (-)
        # 0.98 -
        return l * r
