__all__ = ['PacketMangler', 'in_private', 'radians', 'floats']

from getpass import getuser
import math

def in_private():
    return getuser() == "xenosoz"

def radians(deg_or_none):
    if deg_or_none is None:
        return None
    return math.radians(deg_or_none)
    
def floats(float_or_none):
    if float_or_none is None:
        return 0
    return float(float_or_none)

from .packetmangler import PacketMangler
