from towers.mnesiatower import MnesiaTower
from cars import CompiledCar

class Benchmark(object):
    def __init__(self, game_id):
        self.game_id = game_id
        self.by_tick = {}

    def __call__(self):
        car = self.make_car()
        return car

    @property
    def name(self):
        return self.game_id[:4]

    def make_car(self):
        self.by_tick = {}
        self.past_game_state = None

        tower = MnesiaTower(self.game_id, "fake_key", self, verbose=False)
        tower.run()
        car = CompiledCar(by_tick=dict(self.by_tick))
        return car

    def on_game_state(self, game_state):
        if self.past_game_state is not None:
            future = game_state[None][1]
            present = game_state[None][2]
            throttle = 5.0*future.velocity - 4.9*present.velocity
            throttle = round(throttle, 5)
            self.by_tick[present.game_tick] = throttle
        self.past_game_state = game_state
