import json

class PacketMangler(object):
    def join(self, car_name, key):
        msg = {
            "data": {
                "name": car_name,
                "key": key,
            },
            "msgType": "join",
        }
        return json.loads(json.dumps(msg))

    def your_car(self, color, name, game_id):
        msg = {
            "data": {
                "color": color,
                "name": name,
            },
            "gameId": game_id,
            "msgType": "yourCar",
        }
        return json.loads(json.dumps(msg))
