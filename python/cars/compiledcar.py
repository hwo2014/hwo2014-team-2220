from .basecar import BaseCar

import math

class CompiledCar(BaseCar):
    """Set throttle at certain points, otherwise do nothing"""

    def __init__(self, name=None, by_tick=None, by_track=None):
        if by_tick is None: by_tick = dict()
        if by_track is None: by_track = dict()

        if name is None:
            name = "compiled {}".format(len(by_tick) + len(by_track))
        super(CompiledCar, self).__init__(name=name)

        self.program_by_tick = by_tick
        self.program_by_track = by_track

        self.first_time = True

    def on_game_state(self, game_state):
        future = game_state[None][0]
        present = game_state[None][1]
        past = game_state[None][2]
        ppast = game_state[None][3]
        pppast = game_state[None][4]

        game_tick = present.game_tick
        track_piece_id = present.track_piece_id

        code = self.program_by_tick.get(game_tick, self.program_by_track.get(track_piece_id, None))
        present.throttle_next = 0  # XXXXX
        if isinstance(code, (int, float)):
            if code > 1.0:
                import sys
                sys.stderr.write("throttle: {}\n".format(code))
            present.throttle_next = code
        game_state.fill_estimations()

        # Inspection
        p = present

        if False:
            """Travel distance setting."""
            error = p.travel_distance_hat - p.travel_distance
            if past.throttle_next is None:
                """XXX"""
                past.throttle_next = 0
            estimation_template = ["p.game_tick", "p.acceleration", "past.throttle_next", "past.velocity"]
            validation_template = ["p.game_tick", "error", "p.travel_distance_hat", "p.travel_distance", "p.begin_lane_id", "p.end_lane_id", "p.track_piece_id"]
            validation_template = ["p.game_tick", "error", "p.travel_distance_hat", "p.travel_distance"]
            template = estimation_template
            template = validation_template
            good = True

        if True:
            """Travel angle setting."""
            error = p.in_piece_angle_hat - p.in_piece_angle

            _cf = past.track_piece.inversed_signed_radius * (past.velocity ** 2)
            _A = past.in_piece_angle * past.velocity
            _B = past.in_piece_angular_velocity * past.velocity
            _C = past.in_piece_angular_acceleration * past.velocity
            _D = past.in_piece_angular_jerk * past.velocity

            _cfH = past.track_piece.inversed_signed_radius * (p.velocity_hat ** 2)
            _CH = past.in_piece_angular_acceleration * p.velocity_hat
            _DH = past.in_piece_angular_jerk * p.velocity_hat

            _cfH_ = p.track_piece.inversed_signed_radius * (p.velocity_hat ** 2)
            _cfL = past.lane.inversed_signed_radius * (past.velocity ** 2)

            _cfLH = past.lane.inversed_signed_radius * (p.velocity_hat ** 2)
            _new_1 = past.lane.inversed_signed_radius * p.velocity_hat

            _apple = past.in_piece_angle * past.in_piece_angular_velocity


            _AH = past.in_piece_angle * p.velocity_hat
            _BH = past.in_piece_angular_velocity * p.velocity_hat
            _AAH = past.in_piece_angle * p.velocity_hat**2
            _BBH = past.in_piece_angular_velocity * p.velocity_hat**2

            _new_0 = past.lane.inversed_signed_radius * (p.velocity_hat ** 2)

            # 0.1 -
            # 0.15: 0.854
            # 0.2: 0.885
            # 0.25 0.917
            # 0.28924429832983461: 0.93291147742432434
            # 0.29: 0.9329
            # 0.294: score=0.9332770467360229
            # 0.29470800842067651: 0.93329001112054588
            # 0.29500000000000000: 0.9332921793451441
            # (0.93329310026175327, 0.29521710331580103)
            # (0.93329316239101356, 0.29528993413624594)
            # 0.2955:
            # 0.296: score=0.9332872086035752
            # 0.297: score=0.9332586884795064
            # 0.300: score=0.9330319683784422
            # 0.31: 0.9308
            # 0.35: 0.89
            # 0.4: 0.76
            _m_cfH = (lambda cut:(max(abs(_cfH), cut)-cut) * cmp(_cfH, 0))(0.29528993413624594)
            estimation_template = ["p.game_tick", "p.in_piece_angular_acceleration"]
            # estimation_template += ["past.in_piece_angle"]
            # estimation_template += ["past.in_piece_angular_velocity"]
            # estimation_template += ["past.in_piece_angular_acceleration", "past.in_piece_angular_jerk"]
            # estimation_template += ["_cf"]
            estimation_template += ["_cfH"]
            # estimation_template += ["past.lane.inversed_signed_radius"]
            # estimation_template += ["past.track_piece.track_piece_id"]
            # estimation_template += ["_m_cfH"]
            # estimation_template += ["_new_0"]  #, "_new_1"]
            # estimation_template += ["_cfH_"]
            # estimation_template += ["_cfL"]
            # estimation_template += ["_A"]
            estimation_template += ["_AH"]
            # estimation_template += ["_AAH"]
            # estimation_template += ["_apple"]
            # estimation_template += ["_B"]
            estimation_template += ["_BH"]
            # estimation_template += ["_BBH"]
            # estimation_template += ["_C"]
            # estimation_template += ["_CH"]
            # estimation_template += ["_D"]
            # estimation_template += ["_DH"]

            validation_template = ["p.game_tick", "p.track_piece_id", "error", "p.track_piece.signed_radius", "p.angular_acceleration", "p.in_piece_angle_hat", "p.in_piece_angle", "_cf"]
            validation_template += ["p.begin_lane_id", "p.end_lane_id", "p.throttle_next"]
            validation_template = ["p.game_tick", "error", "_cf", "p.throttle_next"]

            template = validation_template
            template = estimation_template

            def in_(a, b, target_count=None):
                def impl(moment):
                    if (target_count is None or moment.lap_count == target_count-1) and moment.track_piece.track_piece_id in a:
                        return True
                    if (target_count is None or moment.lap_count == target_count) and moment.track_piece.track_piece_id in b:
                        return True
                    return False
                return impl

            in_autobahn = in_([36, 37, 38, 39], [0, 1, 2, 3], target_count=1)

            #0.0202388078861 error -0.0108266330792,0.0094121748069
            in_1st_curve = in_([], [4, 5, 6, 7], target_count=1)  # :

            #0.01647686712362 error -0.0076100609494,0.00886680617422
            in_2nd_curve = in_([], [14, 15, 16, 17], target_count=1)  # :

            # (100, 45): 19, 20, 21, 22
            # (200, 22.5): 23
            # (200, -22.5): 24
            in_s_curve = in_([], [23, 24], target_count=2)
            # 100, switch: 25
            in_3rd_curve = in_([], [19, 20, 21, 22], target_count=1)  # :
            # 0.0220122599883 error -0.0101036586482,0.0119086013401
            in_3rd_all = in_([], [19, 20, 21, 22, 23, 24, 25])

            in_4th_curve = in_([], [26, 27], target_count=1)  # :

            in_5th_curve = in_([], [29, 30], target_count=1)  # :

            in_6th_curve = in_([], [31, 32, 33, 34], target_count=1)  # -:- cf goes down and then up.

            #g = lambda p:(g(p) or g(p))
            g = in_autobahn
            g = lambda p:(in_2nd_curve(p) or in_s_curve(p) or in_4th_curve(p) or in_5th_curve(p) or in_6th_curve(p))
            good = len(set(game_state[None][i].track_piece.signed_radius for i in range(1, 7))) != 1
            good = True
            g = in_([], [23, 24])
            g = in_s_curve
            g = in_2nd_curve
            good = g(past) and g(p)

        if True:
            """ordinary log"""
            if self.first_time:
                self.first_time = False
                if "p.game_tick" in template:
                    title = []
                    for x in template:
                        title.append(x.replace(".", "_"))
                    print("\t".join(title))

            if good:
                template = "\t".join(["{"+x+"}" for x in template])
                print(template.format(**locals()))
        else:
            """log for machine"""
            if self.first_time:
                self.first_time = False
                print("result = []")

            if good:
                result = {}
                for t in template:
                    result[t.replace(".", "_")] = eval(t)
                print("result.append({})".format(result))
