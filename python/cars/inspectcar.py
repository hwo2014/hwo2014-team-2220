from .basecar import BaseCar

class InspectCar(BaseCar):
    """Does nothing and print some results."""

    def __init__(self, name=None):
        super(InspectCar, self).__init__(name=name)

    def on_game_state(self, game_state):
        f = game_state[None][0]  # future
        p = game_state[None][1]  # present
        p.throttle_next = 0.60

        template = ["p.travel_distance", "p.throttle_next"]
        template = "\t".join(["{"+x+"}" for x in template])

        print(template.format(**locals()))
