class BaseCar(object):
    """Just do nothing."""
    def __init__(self, name=None):
        self._name = name

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    def on_game_state(self, game_state):
        pass
