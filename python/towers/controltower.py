import json
from basetower import BaseTower

class ControlTower(BaseTower):
    def __init__(self, socket, key, car, verbose=True):
        super(ControlTower, self).__init__(key=key, car=car, verbose=verbose)
        self.socket = socket

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'yourCar': self.on_your_car,
            'gameInit': self.on_game_init,
            'turboAvailable': self.on_turbo_available,
            'turboEnd': self.on_turbo_end,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'finish': self.on_finish,
            'dnf': self.on_dnf,
            'carPositions': self.on_car_positions,
            'lapFinished': self.on_lap_finished,
            'gameStart': self.on_game_start,
            'gameEnd': self.on_game_end,
            'tournamentEnd': self.on_tournament_end,
            'error': self.on_error,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type = msg['msgType']
            msg_map.get(msg_type, self.on_unexpected_msg)(msg)
            if self.game_state.need_reply_to_the_server:
                self.car.on_game_state(self.game_state)
                self.send_reply_by_game_state()
            line = socket_file.readline()
