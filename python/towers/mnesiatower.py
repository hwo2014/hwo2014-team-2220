import json
import os
from .basetower import BaseTower
from util import PacketMangler

class MnesiaTower(BaseTower):
    def __init__(self, game_id, key, car, verbose=True):
        super(MnesiaTower, self).__init__(key=key, car=car, verbose=verbose)
        self.game_id = game_id
        self.packet = PacketMangler()

    def send(self, msg):
        """Do nothing."""
        pass

    def on_full_car_positions(self, msg):
        if True:
            """Regular packet"""
            for datum in msg['data']:
                datum['angle'] = datum['angleOffset']
                del datum['angleOffset']
                del datum['coordinates']
        else:
            """Super packet"""
            for datum in msg['data']:
                datum['absoluteAngle'] = datum['angle']
                datum['angle'] = datum['angleOffset']
                del datum['angleOffset']
                datum['x'] = datum['coordinates']['x']
                datum['y'] = datum['coordinates']['y']
                del datum['coordinates']

        msg['msgType'] = 'onCarPosition'
        self.on_car_positions(msg)

    def msg_loop(self):
        # quick and dirty implementation
        file_name = os.path.join(os.getcwd(), "../../rankings/tournament/{game_id}".format(game_id=self.game_id))
        with open(file_name, "r") as f:
            game_log_text = f.read()
            msgs = json.loads(game_log_text)

        self.on_join(self.packet.join(car_name=self.car.name, key=self.key))
        self.on_your_car(self.packet.your_car(color='red', name=self.car.name, game_id=self.game_id))

        msg_map = {
            'gameInit': self.on_game_init,
            'dnf': self.on_dnf,
            'fullCarPositions': self.on_full_car_positions,
            'gameStart': self.on_game_start,
            'finish': self.on_finish,
            'lapFinished': self.on_lap_finished,
            'gameEnd': self.on_game_end,
            'tournamentEnd': self.on_tournament_end,
            'turboAvailable': self.on_turbo_available,
            'turboEnd': self.on_turbo_end,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'gameStartsIn': self.on_omitted_msg,
            'liveRanking': self.on_omitted_msg,
            'carVelocities': self.on_omitted_msg,
        }
        for msg in msgs:
            msg_type = msg['msgType']
            msg_map.get(msg_type, self.on_unexpected_msg)(msg)
            if self.game_state.need_reply_to_the_server:
                self.car.on_game_state(self.game_state)
                self.send_reply_by_game_state()
