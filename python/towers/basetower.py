from state import GameState
from operator import itemgetter
from util import radians
import abc
import json

class BaseTower(object):
    """
    Get message from somewhere, dispatch it and update the game state.

    The car gets game_state[:] and modify game_state[0].
    BaseTower sends corresponding message back to the server.

    Dive into the on_car_positions method if you don't have enough time to read this.
    """

    def __init__(self, key, car, verbose=True):
        self.key = key
        self.car = car
        self.verbose = verbose

        # Do not make last_game_state and swap them ANYWHERE.
        # You'll LOSE some important data, yourCar packet doesn't come again for example.
        self.game_state = GameState(history_length=7)

    def log(self, line):
        if self.verbose:
            print(line)

    def warning(self, line):
        import sys
        sys.stderr.write("Warning: {}\n".format(line))

    @abc.abstractmethod
    def send(self, msg):
        """Send message to the (hypothetical) server."""
        pass

    def send_msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send_join(self):
        self.log("< join")
        return self.send_msg("join", {"name": self.car.name,
                                      "key": self.key})

    def send_auto_reply_pong(self):
        """No game_tick specified: unsafe message."""
        self.log("< auto_reply_pong")

        # Yes, it's "ping" by definition. See also: techspec.
        self.msg("ping", {})

    def send_msg_with_game_tick(self, msg_type, data, game_tick):
        self.send(json.dumps({"msgType": msg_type, "data": data, "gameTick": game_tick}))

    def send_throttle(self, throttle, game_tick):
        self.log("< [{game_tick}]throttle({throttle})".format(game_tick=game_tick, throttle=throttle))
        self.send_msg_with_game_tick("throttle", throttle, game_tick)

    def send_switch_lane(self, direction, game_tick):
        self.log("< [{game_tick}]switch_lane({direction})".format(game_tick=game_tick, direction=direction))
        self.send_msg_with_game_tick("switchLane", direction, game_Tick)

    def send_turbo(self, message, game_tick):
        message = str(message)
        self.log("< [{game_tick}]turbo".format(game_tick=game_tick, throttle=throttle))
        self.send_msg_with_game_tick("turbo", message, game_tick)

    def send_noop(self, game_tick):
        self.reply_sent = True
        self.log("< [{game_tick}]noop".format(game_tick=game_tick))
        # Yes, it's "ping" by definition. There's nothing like `noop'. See also: techspec.
        self.send_msg_with_game_tick("ping", {}, game_tick)

    def send_reply_by_game_state(self):
        if not self.game_state.need_reply_to_the_server:
            return
        self.game_state.need_reply_to_the_server = False

        future = self.game_state[None][0]
        present = self.game_state[None][1]
        game_tick = present.game_tick  # use present.game_tick, it's convention.
        if present.throttle_next is not None:
            self.send_throttle(present.throttle_next, game_tick)
            return
        if present.switch_lane_next is not None:
            self.send_switch_lane(present.throttle_next, game_tick)
            return
        if present.turbo_next is not None:
            self.send_turbo(present.turbo_next, game_tick)
            return
        self.send_noop(game_tick)

    def run(self):
        """Run the tower: begin to communicate with the server."""
        self.send_join()
        self.msg_loop()

    def on_join(self, msg):
        """join(None) -> your_car(None) -> game_init(None) -> car_positions(None) -> game_start(0) -> car_positions(1)."""
        self.log("> {msg_type}: {msg}".format(msg_type=msg['msgType'], msg=msg))

        self.game_state.joined = True
        self.game_state.joined_key = msg['data']['key']
        self.game_state.joined_team_name = msg['data']['name']

    def on_your_car(self, msg):
        """join(None) -> your_car(None) -> game_init(None) -> car_positions(None) -> game_start(0) -> car_positions(1)."""
        self.log("> {type_}: {msg}".format(game_id=msg['gameId'], type_=msg['msgType'], msg=msg))

        self.game_state.our_team_color = msg['data']['color']
        self.game_state.our_team_name = msg['data']['name']
        self.game_state.game_id = msg['gameId']

    def on_game_init(self, msg):
        """
        Beginning of the init-start-end chain.

        join(None) -> your_car(None) -> game_init(None) -> car_positions(None) -> game_start(0) -> car_positions(1).
        """
        self.log("> {type_}: {msg}".format(game_id=msg['gameId'], type_=msg['msgType'], msg=msg))

        # Track description.
        self.game_state.track_state.track_id = msg['data']['race']['track']['id']
        self.game_state.track_state.name = msg['data']['race']['track']['name']

        # Lane description.
        lanes = msg['data']['race']['track']['lanes']
        lanes = sorted([(lane['index'], lane['distanceFromCenter']) for lane in lanes], key=itemgetter(0))
        self.game_state.track_state.update_lanes(lanes)

        # Track pieces.
        track_pieces = msg['data']['race']['track']['pieces']
        for track_piece in track_pieces:
            # track_piece is one of following types:
            #   {length}
            #   {length, switch}
            #   {angle, radius}
            #   {angle, radius, switch}
            kw = {
                'length': track_piece.get('length'),
                'switch': track_piece.get('switch'),
                'central_angle': radians(track_piece.get('angle')),
                'radius': track_piece.get('radius'),
            }
            self.game_state.track_state.append(**kw)

        starting_position = msg['data']['race']['track']['startingPoint']['position']
        self.game_state.our_team_starting_position = complex(starting_position['x'], starting_position['y'])
        self.game_state.our_team_starting_angle = radians(msg['data']['race']['track']['startingPoint']['angle'])

        cars = msg['data']['race']['cars']
        for car in cars:
            team_state = self.game_state[car['id']['color']]
            team_state.color = car['id']['color']
            team_state.name = car['id']['name']
            team_state.car_width = car['dimensions']['width']
            team_state.car_length = car['dimensions']['length']
            team_state.car_guide_flag_position = car['dimensions']['guideFlagPosition']

            present = team_state[0]
            present.disqualified = False
            present.crashed = False
            present.finished = False
            present.turbo_available = False
            present.turbo_ticks_left = 0

        self.game_state.max_lap_count = msg['data']['race']['raceSession'].get('laps')
        self.game_state.max_lap_time = msg['data']['race']['raceSession'].get('maxLapTimeMs')
        self.game_state.quick_race = msg['data']['race']['raceSession'].get('quickRace')

    def on_turbo_available(self, msg):
        """car_positions(N-1) -> turbo_available(N) -> car_positions(N)."""
        self.log("> [{tick}]{type_}: {msg}".format(tick=msg['gameTick'], game_id=msg['gameId'], type_=msg['msgType'], msg=msg))

        team_state = self.game_state[None]
        present = team_state[0]

        present.turbo_duration_time = msg['data']['turboDurationMilliseconds']
        present.turbo_duration_ticks = msg['data'].get('turboDurationTicks', int(round(present.turbo_duration_time/1000.*60.)))
        present.turbo_factor = msg['data']['turboFactor']

    def on_crash(self, msg):
        """car_positions(N-1) -> crash(N) -> car_positions(N)."""
        self.log("> [{tick}]{type_}: {msg}".format(tick=msg['gameTick'], game_id=msg['gameId'], type_=msg['msgType'], msg=msg))

        color = msg['data']['color']
        # name = msg['data']['name']
        self.game_state[color][0].crashed = True

    def on_spawn(self, msg):
        """car_positions(N-1) -> spawn(N) -> car_positions(N)."""
        self.log("> [{tick}]{type_}: {msg}".format(tick=msg['gameTick'], game_id=msg['gameId'], type_=msg['msgType'], msg=msg))

        color = msg['data']['color']
        # name = msg['data']['name']
        self.game_state[color][0].crashed = False

    def on_finish(self, msg):
        """car_positions(E) -> finish(None) -> car_positions(None) -> lap_finished(None, race_time=E+1) -> game_end(None) -> tournament_end(None)."""
        self.log("> {type_}: {msg}".format(game_id=msg['gameId'], type_=msg['msgType'], msg=msg))

        color = msg['data']['color']
        # name = msg['data']['name']
        self.game_state[color][0].finished = True

    def on_turbo_end(self, msg):
        """car_positions(N-1) -> turbo_end(N) -> car_positions(N)."""
        self.log("> [{tick}]{type_}: {msg}".format(tick=msg['gameTick'], game_id=msg['gameId'], type_=msg['msgType'], msg=msg))

        color = msg['data']['color']
        # name = msg['data']['name']
        self.game_state[color][0].turbo_ticks_left = 0

    def on_dnf(self, msg):
        """car_positions(N-1) -> dnf(N) -> car_positions(N)."""
        self.log("> [{tick}]{type_}: {msg}".format(tick=msg['gameTick'], game_id=msg['gameId'], type_=msg['msgType'], msg=msg))

        color = msg['data']['car']['color']
        # name = msg['data']['car']['name']
        # reason = msg['data']['reason']
        self.game_state[color][0].disqualified = True

    def on_car_positions(self, msg):
        """
        join(None) -> your_car(None) -> game_init(None) -> car_positions(None) -> game_start(0) -> car_positions(1).
        car_positions(N) -> car_positions(N+1).
        car_positions(N-1) -> turbo_available(N) -> car_positions(N).
        car_positions(N-1) -> turbo_end(N) -> car_positions(N).
        car_positions(N-1) -> crash(N) -> car_positions(N).
        car_positions(N-1) -> spawn(N) -> car_positions(N).
        car_positions(N-1) -> dnf(N) -> car_positions(N).
        car_positions(N) -> lap_finished(N) -> car_positions(N+1).
        car_positions(E) -> finish(None) -> car_positions(None) -> lap_finished(None, race_time=E+1) -> game_end(None) -> tournament_end(None).
        """
        self.log("> [{tick}]{type_}: {msg}".format(tick=msg.get('gameTick'), game_id=msg['gameId'], type_=msg['msgType'], msg=msg))

        game_tick = msg.get('gameTick')  # game_tick=0 once in game_start, and game_tick>0 in car_positions.

        for datum in msg['data']:
            color = datum['id']['color']
            # name = datum['id']['name']
            present = self.game_state[color][0]
            present.game_tick = game_tick
            present.lap_count = datum['piecePosition']['lap']

            present.track_piece_id = datum['piecePosition']['pieceIndex']
            present.track_piece = self.game_state.track_state[present.track_piece_id]

            present.begin_lane_id = datum['piecePosition']['lane']['startLaneIndex']
            present.end_lane_id = datum['piecePosition']['lane']['endLaneIndex']
            present.lane = self.game_state.track_state[present.track_piece_id][present.begin_lane_id, present.end_lane_id]

            present.in_piece_distance = datum['piecePosition']['inPieceDistance']
            present.in_piece_angle = radians(datum['angle'])

        if game_tick is None:
            self.game_state.this_is_initial_state()
        else:
            # game_state[None][0] (=present) becomes game_state[None][1].
            self.game_state.advance()
            self.game_state.need_reply_to_the_server = True

    def on_lap_finished(self, msg):
        """on_car_positions(game_tick=N) comes first, followed by on_lap_finished(game_tick=N)."""
        self.log("> [{tick}]{type_}: {msg}".format(tick=msg.get('gameTick'), game_id=msg['gameId'], type_=msg['msgType'], msg=msg))

        game_tick = msg.get('gameTick', None)  # game_tick is None after finish(None) is called.

        color = msg['data']['car']['color']
        # name = msg['data']['car']['name']
        team_state = self.game_state[color]

        lap_count = msg['data']['lapTime']['lap']
        lap_time = msg['data']['lapTime']['millis']
        lap_ticks = msg['data']['lapTime']['ticks']

        race_lap_count = msg['data']['raceTime']['laps']
        race_ticks = msg['data']['raceTime']['ticks']
        race_time = msg['data']['raceTime']['millis']

        positional_rank = msg['data']['ranking']['overall']
        fastest_lap_rank = msg['data']['ranking']['fastestLap']

    def on_game_start(self, msg):
        """join(None) -> your_car(None) -> game_init(None) -> car_positions(None) -> game_start(0) -> car_positions(1)."""
        self.log("> [{tick}]{type_}: {msg}".format(tick=msg['gameTick'], game_id=msg['gameId'], type_=msg['msgType'], msg=msg))

        game_tick = msg['gameTick']  # game_tick==0 always.
        # assert game_tick == 0  # still dangerous on the race.

        present = self.game_state[None][0]
        present.game_tick = game_tick

        # game_state[None][0] (=present) becomes game_state[None][1].
        self.game_state.advance()
        self.game_state.need_reply_to_the_server = True

    def on_game_end(self, msg):
        """car_positions(E) -> finish(None) -> car_positions(None) -> lap_finished(None, race_time=E+1) -> game_end(None) -> tournament_end(None)."""
        self.log("> {type_}: {msg}".format(game_id=msg['gameId'], type_=msg['msgType'], msg=msg))

        for best_lap in msg['data']['bestLaps']:
            # {car={color, name}, result={millis, ticks, lap}}
            pass

        for result in msg['data']['results']:
            # {car={color, name}, result={laps, ticks, millis}}
            pass

    def on_tournament_end(self, msg):
        """car_positions(E) -> finish(None) -> car_positions(None) -> lap_finished(None, race_time=E+1) -> game_end(None) -> tournament_end(None)."""
        self.log("> {type_}: {msg}".format(game_id=msg['gameId'], type_=msg['msgType'], msg=msg))

        # assert msg['data'] is None
        pass

    def on_error(self, msg):
        self.log("> {type_}: {msg}".format(game_id=msg.get('gameId'), type_=msg.get('msgType'), msg=msg))

    def on_omitted_msg(self, msg):
        # self.log("~ {type_}: {msg}".format(game_id=msg.get('gameId'), type_=msg.get('msgType'), msg=msg))
        pass

    def on_unexpected_msg(self, msg):
        self.warning("? {type_}: {msg}".format(game_id=msg.get('gameId'), type_=msg['msgType'], msg=msg))

    @abc.abstractmethod
    def msg_loop(self):
        """See also: MnesiaTower.msg_loop."""
        pass
