# Backup for safety.

def on_car_positions(self, game_tick, **kw):
    # log_format = "\t".join(["{game_tick}", "{piece_index}", "{signed_lane_radius}", "{lane_index}", "{v}",
    #                         "{a}", "{a_p}", "{v_hat}", "{a_hat}"])

    # log_format = "\t".join(["{signed_inversed_lane_radius}", "{om_pp}", "{err}", "{om_p}", "{v}", "{throttle}"])

    # log_format = "\t".join(["{game_tick}", "{piece_index}", "{signed_lane_radius}", "{lane_index}",
    #                         "{th}", "{om}", "{om_p}", "{om_pp}", "{om_ppp}"])

    # log_format = "\t".join(["{lane_index}", "{v}", "{a}", "{a_p}", "{a_pp}"])

    # log_format = "\t".join(["{game_tick}", "{throttle}", "{signed_inversed_lane_radius}",
    #                         "{th_err}",
    #                         "{th}",
    #                         #"{om}", "{om_p}", "{om_pp}", "{om_ppp}",
    #                         #"{th}", "{om_pp}",
    #                         "{th_hat}", "{om_pp_hat}",
    #                         "{a}",
    #                     ])

    log_format = "\t".join(["{game_tick}", "{throttle}", "{piece_index}", "{signed_inversed_lane_radius}",
                            #"{om_real_ppp}",
                            #"{om_real_pp}",
                            "{om_real_p}", "{om_real}", "{th}", "{th_real}",
                            #"{om_p}", "{om}", "{th}",
                            #"{lane_index}", "{v}",
                            #"{a}", "{a_p}", "{a_pp}", "{th}", "{th_real}", "{om}",
                            #"{v_hat}", "{a_hat}",
                        ])

    if game_tick is None:
        print(log_format)

    data = self.sensor.data()
    last_data = self.sensor.last_data()

    piece_index = data.piece_index
    lane_index = data.lane_index
    lane_radius = data.lane_radius
    inversed_lane_radius = data.inversed_lane_radius
    signed_lane_radius = data.signed_lane_radius
    signed_inversed_lane_radius = data.signed_inversed_lane_radius

    s = data.position[0]
    v = data.position[1]
    a = data.position[2]
    a_p = data.position[3]
    a_pp = data.position[4]

    th = data.angle[0]
    om = data.angle[1]
    om_p = data.angle[2]
    om_pp = data.angle[3]
    om_ppp = data.angle[4]

    th_real = data.real_angle[0]
    om_real = data.real_angle[1]
    om_real_p = data.real_angle[2]
    om_real_pp = data.real_angle[3]
    om_real_ppp = data.real_angle[4]

    a_hat = last_data.throttle * 734.6938775488087 - v * 1.224489795734058
    v_hat = last_data.position[1] + a_hat * self.sensor.time_slice

    om_pp_hat = th * 193.54138499042614
    om_p_hat = last_data.angle[2] + om_pp_hat * self.sensor.time_slice
    om_hat = last_data.angle[1] + om_p_hat * self.sensor.time_slice
    th_hat = last_data.angle[0] + om_hat * self.sensor.time_slice

    th_err = th_hat - th

    data.throttle = 0.60  # XXX
    if game_tick is not None:
        self.execute_program(game_tick, data=self.sensor.data())
    data.throttle = 0.60  # XXX

    throttle = data.throttle

    good = True
    # good = good and data.piece_index in [36, 37, 38, 39, 0, 1, 2]
    # good = good and abs(om_pp) < 10000000
    # good = good and lane_radius == 90
    # good = good and self.sensor.track_id == "keimola"
    if good:
        self.log(log_format.format(**locals()))

    """Succeeding you, father."""
    pass
