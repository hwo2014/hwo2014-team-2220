from collections import defaultdict
from geometry import QuadraticBezier, CubicBezier
import math
import cmath

class SensorData(object):
    """Sensor data for a car."""
    def __init__(self, resolution):
        self.position = (0,) * resolution
        self.angle = (0,) * resolution
        self.real_angle = (0,) * resolution
        self.in_piece_distance = 0
        self.lane_length = 0
        self.piece_index = None
        self.crashed_on_this_tick = False
        self.spawned_on_this_tick = False
        self.track_type = None
        self.throttle = 0.0
        self.signed_inversed_lane_radius = 0

class Sensor(object):
    """Check feed_your_car, feed_game_init, feed_car_positions."""
    def __init__(self, resolution=1):
        self.resolution = resolution
        self.time_slice = 1/60.
        self.body_density = 1.
        self.your_color = None
        self._data = None
        self._crashed = defaultdict(lambda:False)

    def color(self, color=None):
        if color is None:
            return self.your_color
        return color

    def body_mass(self, color=None):
        color = self.color(color)
        assert False and "Not implemented."

    def crashed(self, color=None):
        return self._crashed[self.color(color)]

    def data(self, color=None):
        return self._data[self.color(color)]

    def last_data(self, color=None):
        return self._last_data[self.color(color)]

    def succeeding_you_father(self):
        self._last_data = self._data
        self._data = defaultdict(lambda:SensorData(self.resolution))

    def feed_your_car(self, color, **kw):
        self.your_color = color

    def feed_game_init(self, track_pieces, lanes, track_id, **kw):
        self.succeeding_you_father()

        cumulated_angle = 0
        for track in track_pieces:
            track['startAngle'] = cumulated_angle
            angle = track.get('angle', None)
            if angle is not None:
                angle = math.radians(angle)
                cumulated_angle += angle
            track['endAngle'] = cumulated_angle

        self.track_pieces = track_pieces
        self.lane_offsets = dict()
        for lane in lanes:
            self.lane_offsets[lane['index']] = lane['distanceFromCenter']
        self.track_id = track_id

    def feed_crash(self, color, **kw):
        self._data[color].crashed_on_this_tick = True
        self._crashed[color] = True

    def feed_spawn(self, color, **kw):
        self._data[color].spawned_on_this_tick = True
        self._crashed[color] = False

    def _update_lane_length(self, repo, last_repo):
        repo.track_piece = self.track_pieces[repo.piece_index]
        repo.track_length = repo.track_piece.get('length', None)
        repo.track_start_angle = repo.track_piece['startAngle']
        repo.track_end_angle = repo.track_piece['endAngle']
        repo.lane_index = (repo.start_lane_index + repo.end_lane_index) / 2.

        if repo.track_length is not None:
            """Track piece is a straight line."""
            repo.track_type = "s"
            repo.lane_radius = 0
            repo.track_angle = 0
            if repo.start_lane_index == repo.end_lane_index:
                repo.lane_length = repo.track_length
                return

            # out-in or in-out
            sx = self.lane_offsets[repo.start_lane_index]
            ex = self.lane_offsets[repo.end_lane_index]
            y = repo.track_length

            a = complex(sx, 0)
            b = complex(sx, y/2)
            c = complex(ex, y/2)
            d = complex(ex, y)

            curve = CubicBezier(a, b, c, d)
            repo.lane_length = curve.length()
            return

        """Track piece is an arc."""
        repo.track_type = "a"

        repo.track_angle = math.radians(repo.track_piece['angle'])
        repo.track_radius = repo.track_piece['radius']
        if repo.start_lane_index == repo.end_lane_index:
            lane_index = min(repo.start_lane_index, repo.end_lane_index)
            repo.lane_offset = self.lane_offsets[lane_index]
            repo.lane_radius = repo.track_radius - repo.lane_offset * cmp(repo.track_angle,0)  # right(+) with clockwise(+) = radius(-)
            repo.lane_length = repo.lane_radius * abs(repo.track_angle)
            return

        # out-in or in-out
        start_lane_radius = repo.track_radius - self.lane_offsets[repo.start_lane_index] * cmp(repo.track_angle,0)
        end_lane_radius = repo.track_radius - self.lane_offsets[repo.end_lane_index] * cmp(repo.track_angle,0)
        mid_lane_radius = (start_lane_radius + end_lane_radius) / 2.0
        theta = math.radians(abs(repo.track_angle))

        a = cmath.rect(start_lane_radius, 0)
        b = cmath.rect(mid_lane_radius, theta/2.0)
        c = cmath.rect(end_lane_radius, theta)

        curve = QuadraticBezier(a, b, c)
        repo.lane_length = curve.length()
        repo.lane_radius = repo.lane_length / theta

    def _update_meta_lane_length(self, repo, last_repo):
        repo.inversed_lane_radius = 0 if repo.lane_radius == 0 else 1.0/repo.lane_radius
        repo.signed_lane_radius = repo.lane_radius * cmp(repo.track_angle,0)
        repo.signed_inversed_lane_radius = repo.inversed_lane_radius * cmp(repo.track_angle, 0)

    def _update_position(self, repo, last_repo):
        repo.position = [None] * self.resolution
        repo.position[0] = last_repo.position[0] + repo.in_piece_distance - last_repo.in_piece_distance
        if last_repo.piece_index != repo.piece_index:
            repo.position[0] += last_repo.lane_length 

        for i in xrange(1, self.resolution):
            repo.position[i] = (repo.position[i-1] - last_repo.position[i-1]) / self.time_slice
        repo.position = tuple(repo.position)

    def _update_angle(self, repo, last_repo):
        """Relative angle w.r.t. track piece."""
        repo.angle = [None] * self.resolution
        repo.angle[0] = repo.angle_offset
        for i in xrange(1, self.resolution):
            repo.angle[i] = (repo.angle[i-1] - last_repo.angle[i-1]) / self.time_slice
        repo.angle = tuple(repo.angle)

    def _update_real_angle(self, repo, last_repo):
        """Compute absolute_angle_hat and take as absolute_angle. w.r.t world coordinate."""
        repo.real_angle = [None] * self.resolution
        repo.real_angle[0] = repo.angle_offset + repo.track_start_angle + (repo.in_piece_distance / repo.lane_length) * repo.track_angle

        # if repo.absolute_angle is not None:
        #     # Data from server. It is +90'ed for some reason.
        #     repo.real_angle[0] = repo.angle_offset + repo.absolute_angle - math.pi/2

        for i in xrange(1, self.resolution):
            repo.real_angle[i] = (repo.real_angle[i-1] - last_repo.real_angle[i-1]) / self.time_slice
        repo.real_angle = tuple(repo.real_angle)

    def _feed_car_position(self, datum):
        color = datum['id']['color']
        repo = self._data[color]
        last_repo = self._last_data[color]

        repo.piece_index = datum['piecePosition']['pieceIndex']
        repo.lane = datum['piecePosition']['lane']
        repo.start_lane_index = datum['piecePosition']['lane']['startLaneIndex']
        repo.end_lane_index = datum['piecePosition']['lane']['endLaneIndex']
        repo.in_piece_distance = datum['piecePosition']['inPieceDistance']
        repo.angle_offset = math.radians(datum['angle'])
        repo.absolute_angle = datum.get('absoluteAngle', None)  # Not provided normally.
        if repo.absolute_angle is not None:
            repo.absolute_angle = math.radians(repo.absolute_angle)
        repo.color = datum['id']['color']
        repo.name = datum['id']['name']

        self._update_lane_length(repo=repo, last_repo=last_repo)
        self._update_meta_lane_length(repo=repo, last_repo=last_repo)
        self._update_position(repo=repo, last_repo=last_repo)
        self._update_angle(repo=repo, last_repo=last_repo)
        self._update_real_angle(repo=repo, last_repo=last_repo)

    def feed_car_positions(self, data, game_id, game_tick, **kw):
        self.succeeding_you_father()
        for datum in data:
            self._feed_car_position(datum)
