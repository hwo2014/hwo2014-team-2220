#include <bootstrap>
#include "smartcar"

#include <state/__init__>

struct SmartCar::MiniEngineCtx
{
  MiniEngineCtx() {
    clear();
    need_ice_breaking = true;
  }

  void clear() {
    // need_ice_breaking = true;  // shouldn't cleared by clear.
    meet_alien_done = false;

    // LV1 tests for finding appropriate throttle.
    need_level_1_test = true;
    need_overdrive_test = true;
    need_max_throttle_test = true;
    need_binary_search = true;
    begin_throttle = 0.0;
    end_throttle = 1.0;
  }

  bool need_ice_breaking;
  bool meet_alien_done;

  // level_1_test
  bool need_level_1_test;
  bool need_overdrive_test;
  bool need_max_throttle_test;
  bool need_binary_search;
  double begin_throttle;  // ~ 0.0
  double end_throttle;  // ~ 1.0
};

SmartCar::SmartCar(const string& name, bool verbose)
  : BaseCar(name, verbose),
    alien_visited(false),
    have_gift_for_alien(false),
    mini_engine_ctx(new SmartCar::MiniEngineCtx)
{
  // Base initializtion that would never changed between
  //   1. qualification round.
  //   2. final round.

  // With in-and-out mixed up in the beginning.
  clear_policy();
}

SmartCar::~SmartCar()
{
  if (engine_thread) {
    engine_thread->join();
  }
}

void SmartCar::clear_policy()
{
  // Please use this in inner world.
  // Clear and set throttle 1.0 for tick #0.
  policy.clear();
  mini_engine_ctx.reset(new SmartCar::MiniEngineCtx);

  game.myaction().throttle(1.0);
  policy[0] = game.myaction();
  write_to_shared_policy(policy);
}

void SmartCar::hit_game_end()
{
  // Please use this in inner world.
  clear_policy();
}

void SmartCar::car_mini_engine(shared_ptr<SmartCar::MiniEngineCtx> ctx)
{
  if (ctx->meet_alien_done) {
    ctx->clear();
    ctx->meet_alien_done = false;
    ctx->need_ice_breaking = false;
    if (_verbose) {
      // XXX: need some nice action to deal with.
      game.myaction().throttle(1.0);
      GameState next = game.predict_gamestate(game.state, game.action);
      CarState car = next.cars[game.info.my_color];
      cout << "ps: " << car.ps << ", as: " << car.as << "(" << degrees(car.as) << ")" << endl;
    }
  }

  if (ctx->need_ice_breaking) {
    // Objects are not initialized in this time.
    return;
  }

  if (ctx->need_level_1_test) {
    //
    // LV1: OVERDRIVE
    //
    if (ctx->need_overdrive_test) {
      ctx->need_overdrive_test = false;
      if (game.mycar().nitrous_oxide_count > 0) {
	// Q> Am i still alive after i use turbo?
	// I know using two action in one tick is illegal.
	// But it's just for a test.
	game.myaction().noop();
	game.myaction()._turbo = true;
	game.myaction()._throttle = 1.0;
	GameState next = game.predict_gamestate(game.state, game.action);
	CarState nextcar = next.cars[game.info.my_color];
	if (game.breaking_car_crash_in_n_turn(nextcar) < 0) {
	  // Not gonna die with throttle?
	  game.myaction().turbo();
	  policy[game.state.tick] = game.myaction();
	  write_to_shared_policy(policy);
	  ctx->need_level_1_test = false;
	}
	// see you next blink!
	return;
      }
      // ~0ms used. So, go ahead!
      // pass
    }
    //
    // LV1: MAXTHROTTLE
    //
    if (ctx->need_max_throttle_test) {
      ctx->need_max_throttle_test = false;
      game.myaction().throttle(1.0);
      GameState next = game.predict_gamestate(game.state, game.action);
      CarState nextcar = next.cars[game.info.my_color];
      if (game.breaking_car_crash_in_n_turn(nextcar) < 0) {
	int lane_size = game.info.track.offset_map.size();
	if (game.mycar().reserved_dir == 0 && lane_size > 1) {
	  default_random_engine generator;
	  uniform_int_distribution<int> distribution(0, lane_size-2);
	  int dice_roll = distribution(generator);
	  int dir = (dice_roll >= game.mycar().end_lane_id ? 1 : -1);
	  game.myaction().switch_lane(dir);
	  policy[game.state.tick] = game.myaction();
	  write_to_shared_policy(policy);
	}
	else {
	  // Not gonna die with max throttle? GOGO!
	  policy[game.state.tick] = game.myaction();
	  write_to_shared_policy(policy);
	  ctx->need_level_1_test = false;
	}
      }
      // See you next blink!
      return;
    }
    //
    // LV1: binary search
    //
    if (ctx->need_binary_search) {
      if (abs(ctx->end_throttle - ctx->begin_throttle) < 0.00001) {
	ctx->need_binary_search = false;
	// ~0ms used. Go ahead!
	// pass
      }
      else {
	// Binary search is still useful here.
	double throttle = (ctx->begin_throttle + ctx->end_throttle) / 2.0;
	game.myaction().throttle(throttle);
	GameState next = game.predict_gamestate(game.state, game.action);
	CarState nextcar = next.cars[game.info.my_color];
	if (game.breaking_car_crash_in_n_turn(nextcar) < 0) {
	  // Not gonna die? Throttle up!
	  ctx->begin_throttle = throttle;
	}
	else {
	  // Die? Throttle down!
	  ctx->end_throttle = throttle;
	}
	// See you next blink!
	return;
      }
    }
  } // LV1 tests for finding appropriate throttle.

} // End of car_mini_engine.

void SmartCar::car_engine()
{
  int dumb_counter = 0;
  bool finished = false;
  while (!finished) {
    // Busy working.
    if (alien_visited) {
      // Alien visited and I have a new game.
      game = read_from_shared_game();

      if (policy.find(game.state.tick) == policy.end()) {
	// Only fill the missing policies.
	// Fallback behavior: Set lowest possible value.
	double throttle = mini_engine_ctx->begin_throttle;
	throttle = min(max(0.0, throttle), 1.0);

	game.myaction().throttle(throttle);
	policy[game.state.tick] = game.myaction();
	write_to_shared_policy(policy);
      }
      mini_engine_ctx->meet_alien_done = true;
    }
    if (!finished) {
      car_mini_engine(mini_engine_ctx);
    }
  }
}

void SmartCar::run()
{
  // From the thread in outer world.
  // Do nothing.
  engine_thread.reset(new thread(&SmartCar::car_engine, this));
}

void SmartCar::thread_join()
{
  engine_thread->join();
  engine_thread.reset();
}

void SmartCar::on_game(Game& alien_game)
{
  // From the thread in outer world.
  // USE alien_game, alien_policy ONLY FOR HERE.
  write_to_shared_game(alien_game);

  bool satisfied = false;
  bool claimed = false;
  int mana = 3000;
  while (!satisfied) {
    policy_type alien_policy = read_from_shared_policy();
    auto f = alien_policy.find(alien_game.state.tick);
    if (f != alien_policy.end()) {
      alien_game.myaction() = f->second;
      satisfied = true;
    }
    else {
      if (!claimed) {
	mana -= 1;
	if (mana <= 0) {
	  cout << "Not enough Alien mana at #" << alien_game.state.tick << "." << endl;
	  claimed = true;
	}
      }
    }
  }
}

Game SmartCar::read_from_shared_game()
{
  lock_guard<mutex> read_lock(shared_game_read_mutex);
  alien_visited = false;
  return shared_game;
}

void SmartCar::write_to_shared_game(Game game)
{
  lock_guard<mutex> read_lock(shared_game_read_mutex);
  lock_guard<mutex> write_lock(shared_game_write_mutex);
  //swap(shared_game, game);
  shared_game = game;
  alien_visited = true;
}

SmartCar::policy_type SmartCar::read_from_shared_policy()
{
  lock_guard<mutex> read_lock(shared_policy_read_mutex);
  have_gift_for_alien = false;
  return shared_policy;
}

void SmartCar::write_to_shared_policy(policy_type policy)
{
  lock_guard<mutex> read_lock(shared_policy_read_mutex);
  lock_guard<mutex> write_lock(shared_policy_write_mutex);
  //swap(shared_policy, policy);
  shared_policy = policy;
  have_gift_for_alien = true;
}

// void a_star() {
//   node start;
//   vector<node> open;
//   vecotr<node> closed;

//   open.push_back(start);

//   while(!open.empty()) {
//     // F = G + H
//     // G: movement cost [begin, x)
//     // H: estimated movement cost [x, end): heuristic.
//     node current = open.pop(); // with lowest F cost. use binary heap.
//     closed.push_back(current);
//     if (current == target) {
//       return success;
//     }

//     for (node n : current.sdf) {
//       if (n is in closed) { continue; }
//       if (n is not in open) {
// 	open.push_back(n);
// 	n.parent = current;
// 	n.record_f_g_h_score();
//       }
//       else {
// 	node old_n = get_old_n();
// 	if (n.is_better_than(old_n)) {
// 	  old_n.parent = current;
// 	  old_n.update_f_g_score(); // resort open list.
// 	}
//       }
    
//     }

//   }
