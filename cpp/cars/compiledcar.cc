#include <bootstrap>
#include "compiledcar"

#include <state/__init__>

CompiledCar::CompiledCar(const string& name, bool verbose)
  : BaseCar(name, verbose)
{
}

void CompiledCar::on_game(Game& game)
{
  game.myaction().throttle(0.5);
}

void CompiledCar::run()
{
  // Do nothing.
}
