#include <bootstrap>
#include "basecar"

#include <state/__init__>

BaseCar::BaseCar(const string& name, bool verbose)
  : _name(name)
  , _verbose(verbose)
{
}

const string BaseCar::name() const
{
  return _name;
}

void BaseCar::info(const string& line)
{
  if (_verbose) {
    cout << line << endl;
  }
}

void BaseCar::log(const string& line)
{
  cout << line << endl;
}

void BaseCar::warning(const string& line)
{
  cout << line << endl;
}
