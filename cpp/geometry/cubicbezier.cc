#include <bootstrap>
#include "cubicbezier"

CubicBezier::CubicBezier(point_type a, point_type b, point_type c, point_type d)
  : a(a), b(b), c(c), d(d)
  , abc(a, b, c)
  , bcd(b, c, d)
  , length(_length())
{
}

point_type CubicBezier::operator()(double t) const
{
  return abc(t) * (1-t) + bcd(t) * t;
}

double CubicBezier::_length() const
{
  int line_segments = 100;

  double len = 0;
  complex<double> u = (*this)(0.0);
  for (int i = 1; i <= line_segments; ++i) {
    double t = double(i) / line_segments;
    complex<double> v = (*this)(t);
    len += abs(v - u);
    u = v;
  }

  // NB(xenosoz): Have no idea why discount < 1.
  double discount = 0.993045;
  return len * discount;
}
