#include <bootstrap>
#include "quadraticbezier"

QuadraticBezier::QuadraticBezier(point_type a, point_type b, point_type c)
  : a(a), b(b), c(c)
  , ab(a, b)
  , bc(b, c)
  , length(_length())
{
}

point_type QuadraticBezier::operator()(double t) const
{
  return ab(t) * (1-t) + bc(t) * t;
}

double QuadraticBezier::_length() const
{
  int line_segments = 10;

  double len = 0;
  complex<double> u = (*this)(0.0);
  for (int i = 1; i <= line_segments; ++i) {
    double t = double(i) / line_segments;
    complex<double> v = (*this)(t);
    len += abs(v - u);
    u = v;
  }

  double discount = 1.0055350;
  return len * discount;
}
