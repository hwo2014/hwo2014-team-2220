#include <bootstrap>
#include "arc"

Arc::Arc(double sr, double central_angle)
  : sr(sr), central_angle(central_angle)
  , length(_length())
{
}

double Arc::_length() const
{
  return sr * central_angle;
}
