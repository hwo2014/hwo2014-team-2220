#include <bootstrap>
#include "linearbezier"

LinearBezier::LinearBezier(point_type a, point_type b)
  : a(a), b(b)
  , length(_length())
{
}

point_type LinearBezier::operator()(double t) const
{
  return a * (1-t) + b * t;
}

double LinearBezier::_length() const
{
  return abs(b - a);
}
