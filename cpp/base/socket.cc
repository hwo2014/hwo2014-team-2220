#include <bootstrap>
#include "socket"

Socket::Socket(const std::string& host, const std::string& port)
  : io_service(new boost::asio::io_service),
    tcp_socket(new tcp::socket(*io_service)),
    response_buf(new boost::asio::streambuf)
{

  tcp::resolver resolver(*io_service);
  tcp::resolver::query query(host, port);
  boost::asio::connect(*tcp_socket, resolver.resolve(query));
  response_buf->prepare(8192);
}

void Socket::close()
{
  tcp_socket->close();
}

json Socket::receive_response(boost::system::error_code& error)
{
  auto len = boost::asio::read_until(*tcp_socket, *response_buf, "\n", error);
  if (error) {
    return json();
  }

  auto buf = response_buf->data();
  string reply(boost::asio::buffers_begin(buf), boost::asio::buffers_begin(buf) + len);
  response_buf->consume(len);

  return json::parse_string(reply);
}

void Socket::send_requests(const vector<json>& msgs)
{
  output_format format;
  format.escape_all_non_ascii(true);

  boost::asio::streambuf request_buf;
  std::ostream s(&request_buf);
  for (const auto& m : msgs) {
    m.to_stream(s, format);
    s << std::endl;
  }

  tcp_socket->send(request_buf.data());
}
