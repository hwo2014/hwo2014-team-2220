#include <bootstrap>
#include "msgparser"

#include <state/__init__>

MsgParser::MsgParser(Game& game, bool verbose)
  : _game(game)
{
  _verbose = verbose;

  // XXX: We can make it const. Should update emacs c++-mode for C++11.
  action_map = {
    {"join", &MsgParser::on_join},
    {"yourCar", &MsgParser::on_your_car},
    {"gameInit", &MsgParser::on_game_init},
    {"carPositions", &MsgParser::on_car_positions},
    {"gameStart", &MsgParser::on_game_start},

    {"finish", &MsgParser::on_finish},
    {"gameEnd", &MsgParser::on_game_end},
    {"tournamentEnd", &MsgParser::on_tournament_end},

    {"crash", &MsgParser::on_crash},
    {"spawn", &MsgParser::on_spawn},
    {"turboAvailable", &MsgParser::on_turbo_available},
    {"turboStart", &MsgParser::on_turbo_start},
    {"turboEnd", &MsgParser::on_turbo_end},

    {"lapFinished", &MsgParser::on_lap_finished},
    {"dnf", &MsgParser::on_dnf},

    {"error", &MsgParser::on_error},
  };
}

void MsgParser::feed_msg(json& msg)
{
  const auto& msg_type = msg["msgType"].as_string();
  auto f = action_map.find(msg_type);

  if(f != action_map.end()) {
    (f->second)(this, msg);
    return;
  }
  on_unexpected_msg(msg);
}

void MsgParser::on_join(json& msg)
{
  _on_tickless(msg);
  _game.info.joined = true;
  _game.info.joined_key = str(msg["data"]["key"]);
  _game.info.joined_team_name = str(msg["data"]["name"]);
}

void MsgParser::on_your_car(json& msg)
{
  _on_tickless(msg);
  _game.info.my_color = str(msg["data"]["color"]);
  _game.info.my_name = str(msg["data"]["color"]);
  _game.info.game_id = str(msg["gameId"]);
}

void MsgParser::on_game_init(json& msg)
{
  _on_tickless(msg);

  // Track description.
  TrackInfo& track = _game.info.track;
  track.track_id = str(msg["data"]["race"]["track"]["id"]);
  track.name = str(msg["data"]["race"]["track"]["name"]);

  // Lane description.
  track.offset_map.clear();
  for (auto& lane : iter(msg["data"]["race"]["track"]["lanes"])) {
    int id = num(lane["index"]);
    track.offset_map[id] = real(lane["distanceFromCenter"]);
  }

  // Track pieces.
  track.pieces.clear();
  for (auto& piece : iter(msg["data"]["race"]["track"]["pieces"])) {
    PieceInfo p;
    p.length = real(piece.get("length"));
    p.switch_ = truth(piece.get("switch"));
    p.sa = radians(real(piece.get("angle")));
    p.sr = real(piece.get("radius")) * sgn(p.sa);
    p.isr = (p.sr != 0.0 ? (1.0/p.sr) : 0.0);

    // (lane_id, lane_id)
    for (auto& kv : track.offset_map) {
      int lane_id = kv.first;
      double offset = kv.second;
      pair<int,int> key(lane_id, lane_id);
      p.lanes[key] = LaneInfo(p.length, p.sa, p.sr, offset);
    }
    
    if (p.switch_) {
      // (begin_lane_id, end_lane_id)
      for (auto& kv : track.offset_map) {
	int begin_lane_id = kv.first;
	double begin_offset = kv.second;
	for (auto& end_lane_id : {begin_lane_id-1, begin_lane_id+1}) {
	  auto f = track.offset_map.find(end_lane_id);
	  if (f != track.offset_map.end()) {
	    continue;
	  }
	  double end_offset = (*f).second;
	  pair<int,int> key(begin_lane_id, end_lane_id);
	  p.lanes[key] = LaneInfo(p.length, p.sa, p.sr, begin_offset, end_offset);
	}
      }
    }
    track.pieces.push_back(p);
  }

  // Starting positions.
  {
    json starting_point = msg["data"]["race"]["track"]["startingPoint"];
    _game.info.my_starting_pos = complex<double>(real(starting_point["position"].get("x")), real(starting_point["position"].get("y")));
    _game.info.my_starting_angle = radians(real(starting_point["angle"]));
  }

  // Cars
  for (auto& car : iter(msg["data"]["race"]["cars"])) {
    TeamInfo& team = _game.info.teams[str(car["id"]["color"])];
    team.color = str(car["id"]["color"]);
    team.name = str(car["id"]["name"]);
    team.car_width = real(car["dimensions"]["width"]);
    team.car_length = real(car["dimensions"]["length"]);
    team.car_guide_flag_pos = real(car["dimensions"]["guideFlagPosition"]);
  }

  _game.info.max_lap_count = num(msg["data"]["race"]["raceSession"].get("laps"));
  _game.info.max_lap_time = num(msg["data"]["race"]["raceSession"].get("maxLapTimeMs"));
  _game.info.quick_race = truth(msg["data"]["race"]["raceSession"].get("quickRace"));
}

void MsgParser::on_car_positions(json& msg)
{
  _on_tickful(msg);
  json data = msg["data"];

  // game_tick = 0 once in game_start, and game_tick > 0
  json tick_json = msg.get("gameTick");

  // NB(xenosoz): None -> 0.
  int tick = num(tick_json);
  _game.state.tick = tick;

  for (auto& datum : iter(data)) {
    const auto& color = str(datum["id"]["color"]);
    const auto& name = str(datum["id"]["name"]);

    CarState& car = _game.state.cars[color];
    CarState old = car;
    car.color = color;
    car.tick = tick;
    car.lap_count = num(datum["piecePosition"]["lap"]);
    car.piece_id = num(datum["piecePosition"]["pieceIndex"]);
    car.begin_lane_id = num(datum["piecePosition"]["lane"]["startLaneIndex"]);
    car.end_lane_id = num(datum["piecePosition"]["lane"]["endLaneIndex"]);
    car.in_lane_p = real(datum["piecePosition"]["inPieceDistance"]);
    car.in_lane_a = radians(real(datum["angle"]));

    // Let's advance!
    car.ps += car.in_lane_p - old.in_lane_p;
    if (car.piece_id != old.piece_id) {
      pair<int,int> lane_key(old.begin_lane_id, old.end_lane_id);
      int piece_key = old.piece_id;
      car.ps += _game.info.track.pieces[piece_key].lanes[lane_key].length;
    }
    car.pv = car.ps - old.ps;
    car.pa = car.pv - old.pv;

    if (tick == 1) {
      _game.info.power = max(_game.info.power, car.ps);
    }

    car.as = car.in_lane_a;
    car.av = car.as - old.as;
    car.aa = car.av - old.av;
  }

  if (!tick_json.is_null()) {
    _game.state.need_reply = true;
  }
}

void MsgParser::on_game_start(json& msg)
{
  _on_tickful(msg);

  int tick = num(msg["gameTick"]);  // tick == 0 always.
  _game.state.tick = tick;
  _game.state.need_reply = true;
}

void MsgParser::on_finish(json& msg)
{
  _on_tickless(msg);

  const auto& color = str(msg["data"]["color"]);
  CarState& car = _game.state.cars[color];
  car.finished = true;
}

void MsgParser::on_game_end(json& msg)
{
  _on_tickless(msg);

  const json& best_laps = msg["data"]["bestLaps"];
  for (auto it = best_laps.begin_elements(); it != best_laps.end_elements(); ++it) {
    // {car={color, name}, result={millis, ticks, lap}}
    // Do nothing.
  }

  const json& results = msg["data"]["results"];
  for (auto it = best_laps.begin_elements(); it != best_laps.end_elements(); ++it) {
    // {car={color, name}, result={laps, ticks, millis}}
    // Do nothing.
  }
}

void MsgParser::on_tournament_end(json& msg)
{
  _on_tickless(msg);
  // assert msg["data"].is_null()
}

void MsgParser::on_crash(json& msg)
{
  _on_tickful(msg);

  const auto& color = str(msg["data"]["color"]);
  const auto& name = str(msg["data"]["name"]);

  CarState& car = _game.state.cars[color];
  car.crashed = true;
}

void MsgParser::on_spawn(json& msg)
{
  _on_tickful(msg);

  const auto& color = str(msg["data"]["color"]);
  const auto& name = str(msg["data"]["name"]);

  CarState& car = _game.state.cars[color];
  car.crashed = false;
}

void MsgParser::on_turbo_available(json& msg)
{
  _on_tickful(msg);

  CarState& car = _game.mycar();
  car.nitrous_oxide_count += 1;

  // This setting is global by definition.
  _game.info.turbo_time = num(msg["data"]["turboDurationMilliseconds"]);
  _game.info.turbo_ticks = num(msg["data"]["turboDurationTicks"]);
  _game.info.turbo_factor = real(msg["data"]["turboFactor"]);
}

void MsgParser::on_turbo_start(json& msg)
{
  _on_tickful(msg);

  const auto& color = str(msg["data"]["color"]);
  const auto& name = str(msg["data"]["name"]);
  CarState& car = _game.state.cars[color];
  car.nitrous_oxide_count -= 1;
  car.turbo_ticks_left = _game.info.turbo_ticks;
}

void MsgParser::on_turbo_end(json& msg)
{
  _on_tickful(msg);

  const auto& color = str(msg["data"]["color"]);
  const auto& name = str(msg["data"]["name"]);
  CarState& car = _game.state.cars[color];

  cout << "car.turobo_ticks_left: " << car.turbo_ticks_left << " -> " << 0 << endl;
  car.turbo_ticks_left = 0;
}

void MsgParser::on_lap_finished(json& msg)
{
  _on_tickful(msg);

  const auto& color = str(msg["data"]["car"]["color"]);
  const auto& name = str(msg["data"]["car"]["name"]);
  TeamState& team = _game.state.teams[color];
  team.color = color;

  team.lap_count = num(msg["data"]["lapTime"]["lap"]);
  team.lap_time = num(msg["data"]["lapTime"]["millis"]);
  team.lap_ticks = num(msg["data"]["lapTime"]["ticks"]);

  team.race_lap_count = num(msg["data"]["raceTime"]["laps"]);
  team.race_ticks = num(msg["data"]["raceTime"]["ticks"]);
  team.race_time = num(msg["data"]["raceTime"]["millis"]);

  team.positional_rank = num(msg["data"]["ranking"]["overall"]);
  team.fastest_lap_rank = num(msg["data"]["ranking"]["fastestLap"]);
}

void MsgParser::on_dnf(json& msg)
{
  _on_tickful(msg);

  const auto& color = str(msg["data"]["car"]["color"]);
  const auto& name = str(msg["data"]["car"]["name"]);
  TeamState& team = _game.state.teams[color];
  team.disqualified = true;
  team.dnf_reason = str(msg["data"]["reason"]);
}

void MsgParser::on_error(json& msg)
{
  _on_tickless(msg);
}

void MsgParser::on_omitted_msg(json& msg)
{
  // Do nothing.
}

void MsgParser::on_unexpected_msg(json& msg)
{
  const auto& type = str(msg.get("msgType"));
  cout << "~ " << type << ": " << msg << endl;
}

void MsgParser::_on_before_msg(json& msg)
{
  _game.state.need_reply = false;
}

void MsgParser::_on_after_msg(json& msg)
{
  // do nothing.
}

void MsgParser::_on_tickless(json& msg)
{
  if (!_verbose) { return; }
  const auto& type = str(msg.get("msgType"));
  cout << "> " << type << ": " << msg << endl;
}

void MsgParser::_on_tickful(json& msg)
{
  if (!_verbose) { return; }
  const auto& type = str(msg.get("msgType"));
  const auto& tick = num(msg.get("gameTick"));
  cout << "> [" << tick << "]" << type << ": " << msg << endl;
}
