#include <bootstrap>
#include "carstate"

CarState::CarState()
  : color()
  , tick(-1)

  , finished(false)
  , crashed(false)
  , nitrous_oxide_count(0)
  , turbo_ticks_left(0)
  , reserved_dir(0)

  , lap_count(0)
  , piece_id(0)
  , begin_lane_id(0)
  , end_lane_id(0)
  , in_lane_p(0.0)
  , in_lane_a(0.0)

  , ps(0.0)
  , pv(0.0)
  , pa(0.0)

  , as(0.0)
  , av(0.0)
  , aa(0.0)
{
}
