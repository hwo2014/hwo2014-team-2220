#include <bootstrap>
#include "game"

#include <state/__init__>

Game::Game()
{
}

GameState Game::predict_gamestate(GameState state, GameAction action)
{
  // NB(xenosoz): intentional shadowing coming;
  //   (GameState state) is shadowing Game::state.
  //   (GameAction action) is shadowing Game::action.

  GameState later;
  later.tick = state.tick + 1;
  later.need_reply = false;

  for(auto& kv : state.cars) {
    const string& now_color = kv.first;

    TeamState& now_team = state.teams[now_color];
    CarState& now_car = kv.second;
    if (now_team.disqualified) {
      later.cars[now_color] = now_car;
      continue;
    }
    CarAction& act = action.cars[now_color];
    later.cars[now_color] = predict_carstate(now_car, act);
  }

  for(auto& kv : state.teams) {
    const string& now_color = kv.first;

    TeamState& now_team = kv.second;
    if (now_team.disqualified) {
      later.teams[now_color] = now_team;
      continue;
    }
    CarState& car = later.cars[now_color];
    later.teams[now_color] = predict_teamstate(now_team, car);
  }

  return later;
}

CarState Game::predict_carstate(CarState now, CarAction act)
{
  if (now.finished) { return now; }

  CarState later = now;
  later.tick += 1;

  // [1] turbo.
  if (now.nitrous_oxide_count > 0 && act._turbo) {
    later.nitrous_oxide_count = now.nitrous_oxide_count - 1;
    later.turbo_ticks_left = info.turbo_ticks;
  }
  else {
    later.turbo_ticks_left = max(0, later.turbo_ticks_left-1);
  }

  double turbo_factor = (now.turbo_ticks_left>0 ? info.turbo_factor: 1.0);

  // [2/3] throttle.
  double throttle = max(act._throttle, 0.0);
  later.pa = info.power * turbo_factor * throttle - info.drag * now.pv;
  later.pv = now.pv + later.pa;
  later.ps = now.ps + later.pv;

  PieceInfo& now_piece = info.track.pieces[now.piece_id];
  LaneInfo& now_lane = now_piece.lanes[make_pair(now.begin_lane_id, now.end_lane_id)];

  // Move!
  later.in_lane_p += later.pv;

  if (later.in_lane_p >= now_lane.length) {
    // Move far enough: go to the next lane.
    later.in_lane_p -= now_lane.length;
    later.piece_id += 1;
    if (later.piece_id >= info.track.pieces.size()) {
      // len(pieces) becomes 1 lap count.
      later.piece_id -= info.track.pieces.size();
      later.lap_count += 1;
    }
    if (now_piece.switch_ && now.reserved_dir != 0) {
      // Lane change.
      later.begin_lane_id = now.end_lane_id + now.reserved_dir;
      later.begin_lane_id = min(max(0, later.begin_lane_id), int(info.track.offset_map.size())-1);
      later.end_lane_id = later.begin_lane_id;
      later.reserved_dir = 0;  // star(*)
    }
  }

  // [3/3] lane change.
  if (act._dir != 0 && later.reserved_dir != 0) {  // star(*)
    // reserved_dir != 0 check for `second command dumb' rule.
    later.reserved_dir = act._dir;
  }

  double now_track_isr = info.track.pieces[now.piece_id].isr;
  double F_c = later.pv * later.pv * now_track_isr;
  double F_c_tire = info.F_c_tire;
  double F_c_net = max(F_c - F_c_tire, 0.0) + min(F_c + F_c_tire, 0.0);

  // error ~0.195484 degree/tick. it was best, but i can't stand fit_intercept=True.
  // later.aa = -0.0008897497714358326;
  // later.aa += -0.0003713190242523886 * now.as;
  // later.aa += -0.09783015355817692 * now.av;
  // later.aa += 0.039425779180993 * F_c_net;
  // later.aa += -0.0011407201527727179 * now.as * later.pv;

  // later.aa = 0.0;
  // later.aa += 0.0063824376742064644 * now.as;
  // later.aa += -0.09047875906123885 * now.av;
  // later.aa += 0.03527788705218809 * F_c_net;
  // later.aa += -0.00204741072652501 * now.as * later.pv;

  // Hybrid.
  if (now_track_isr == 0.0) {
    // On the straight line
    later.aa = 0.0;
    later.aa += -0.01 * now.as;
    later.aa += -0.1 * now.av;
  }
  else {
    later.aa = 0.0;
    later.aa += 0.04 * F_c_net;
    later.aa += -0.002 * now.as * later.pv;
    later.aa += -0.02 * now.av * later.pv;
  }

  later.av = now.av + later.aa;
  later.as = now.as + later.av;
  later.in_lane_a = later.as;

  if (abs(later.in_lane_a) > info.crash_as) {
    later.crashed = true;
  }
  if (!later.crashed && info.max_lap_count > 0 && later.lap_count >= info.max_lap_count) {
    later.finished = true;
  }

  return later;
}

TeamState Game::predict_teamstate(TeamState now, CarState car)
{
  // XXX
  return now;
}

int Game::breaking_car_crash_in_n_turn(CarState now, int lookahead)
{
  if (lookahead < 0) {
    lookahead = info.crash_lookahead_count;
  }

  CarAction act;
  act.throttle(0.0);
  for (int n = 0; n < lookahead; ++n) {
    if (now.crashed) {
      cout << "[" << now.tick << "]crash after " << n << " turns." << endl;
      return n;
    }
    if (now.finished) { return -100; }
    if (now.pv < 0.01) { return -1; }
    now = predict_carstate(now, act);
  }
  return -1;
}

CarAction& Game::myaction()
{
  return action.cars[info.my_color];
}

CarState& Game::mycar()
{
  return state.cars[info.my_color];
}
