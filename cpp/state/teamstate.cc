#include <bootstrap>
#include "teamstate"

TeamState::TeamState()
  : color()
  , disqualified(false)
  , dnf_reason()

  , lap_count(0)
  , lap_time(0)
  , lap_ticks(0)

  , race_lap_count(0)
  , race_ticks(0)
  , race_time(0)

  , positional_rank(0)
  , fastest_lap_rank(0)
{
}
