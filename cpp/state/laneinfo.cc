#include <bootstrap>
#include "laneinfo"

LaneInfo::LaneInfo()
  : _type(none_type)
  , length(0.0)
  , sr(0.0)
  , isr(0.0)
{
}

LaneInfo::LaneInfo(double length, double sa, double sr, double offset)
{
  if (length == 0.0) {
    // Arc without switch.
    _type = arc_type;
    // clockwise(+), right(+) = reduce(-)
    double signed_radius = sr - offset;
    double central_angle = sa;
    arc = Arc(signed_radius, central_angle);
    this->length = abs(arc.length);
    this->sr = signed_radius;
    this->isr = (signed_radius != 0.0 ? 1.0/signed_radius : 0.0);
    return;
  }

  // Straight without switch.
  _type = linear_type;
  point_type a(offset, 0);
  point_type b(offset, length);
  linear = LinearBezier(a, b);
  this->length = abs(linear.length);
  this->sr = 0.0;
  this->isr = 0.0;
}

LaneInfo::LaneInfo(double length, double sa, double sr, double begin_offset, double end_offset)
{
  if (length == 0.0) {
    // Arc with switch.
    _type = quadratic_type;
    double begin_sr = sr - begin_offset;
    double end_sr = sr - end_offset;
    double mid_sr = (begin_sr + end_sr) / 2.0;
    point_type a = polar(begin_sr, 0.0);
    point_type b = polar(mid_sr, sa/2.0);
    point_type c = polar(end_sr, sa);
    quadratic = QuadraticBezier(a, b, c);
    this->length = abs(quadratic.length);
    this->sr = sr;
    this->isr = (sr != 0.0 ? 1.0/sr : 0.0);
    return;
  }

  // Straight line with switch.
  _type = cubic_type;
  point_type a(begin_offset, 0);
  point_type b(begin_offset, length/2.0);
  point_type c(end_offset, length/2.0);
  point_type d(end_offset, length);
  cubic = CubicBezier(a, b, c, d);
  this->length = abs(cubic.length);
  // XXX: isr != 0.0 maybe.
  this->sr = 0.0;
  this->isr = 0.0;
}
