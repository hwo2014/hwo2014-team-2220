#include <bootstrap>
#include "gameinfo"

GameInfo::GameInfo()
  : joined(false)
  , joined_key()
  , joined_team_name()

  , my_color()
  , my_name()
  , game_id()

  , my_starting_pos(0.0, 0.0)
  , my_starting_angle(90.0)

  , max_lap_count(-1)  // 0: inf
  , max_lap_time(-1)  // 0: inf
  , quick_race(false)

  , turbo_time(0)
  , turbo_ticks(0)
  , turbo_factor(1.0)

  , power(0.2)
  , drag(0.02)
    //, F_c_tire(0.29528993413624594)
  , F_c_tire(0.01)

  , crash_as(radians(60.0 * 0.95))
  , crash_lookahead_count(200)
    // estimated crash after 150 turn with throttle = 0.0
    // due to angular acceleration error.
{}
