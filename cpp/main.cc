#include <bootstrap>

#include <base/socket>
#include <cars/compiledcar>
#include <cars/smartcar>
#include <towers/controltower>
#include <towers/mnesiatower>

void be_the_good_girl(string host, string port, string name, string key)
{
  // CODE FOR THE CONTEST. BE CAREFUL!

  cerr << "Host: " << host << ", port: " << port << ", name: " << name << ", key: *" << endl;

  Socket socket(host, port);
  bool verbose = in_private();
  SmartCar car(name, verbose);
  ControlTower tower(socket, key, car, verbose);
  car.run();
  tower.run();
  socket.close();
}

void be_the_bad_girl(string host, string port, string name, string key)
{
    cerr << "Host: " << host << ", port: " << port << ", name: " << name << ", key: *" << endl;

    Socket socket(host, port);
    bool verbose = in_private();
    SmartCar car(name, verbose);
    ControlTower tower(socket, key, car, verbose);
    car.run();
    tower.run();
    socket.close();
}

void go_play_outside(string host, string port, string name, string key)
{
  string keimola = "5ba295bb-f343-4ee6-b45a-92b3ad85bbec";
  string germany = "1f5238db-93f9-4920-a994-75d929e627cf";
  string U_S_A = "e7df0508-2dfd-4810-a5c7-81c6ba4255e8";
  string save_the_bacon = "240809da-876f-4424-b247-dc815f60f4ac";

  string game_id = keimola;
  // car = Benchmark(game_id)();

  // verbose = false;
  // tower = MnesiaTower(game_id, key, car, verbose);
  // tower.run();
}

int wrapped_main(int argc, const char* argv[])
{
  const std::string host(argv[1]);
  const std::string port(argv[2]);
  const std::string name(argv[3]);
  const std::string key(argv[4]);
  if (in_private()) {
    //go_play_outside(host, port, name, key);
    be_the_bad_girl(host, port, name, key);
  }
  else {
    be_the_good_girl(host, port, name, key);
  }
  return 0;
}

int main(int argc, const char* argv[])
{
  if (argc != 5) {
    std::cerr << "Usage: ./run host port botname botkey" << std::endl;
    return 1;
  }
  try {
    return wrapped_main(argc, argv);
  }
  catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 2;
  }
}
