#include "__init__"
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <string>

using namespace std;

namespace util
{
  bool in_private()
  {
    uid_t id = 0;
    struct passwd *pwd = getpwuid(geteuid());
    if (!pwd) { return false; }
    if (string(pwd->pw_name) != "xenosoz") { return false; }
    return true;
  }

  double radians(double degrees)
  {
    return degrees * pi / 180.0;
  }

  double degrees(double radians)
  {
    return radians * 180.0 / pi;
  }

  const string str(const json& json, const string& default_)
  {
    if (json.is_null()) { return default_; }
    return json.as<string>();
  }

  bool truth(const json& json, bool default_)
  {
    if (json.is_null()) { return default_; }
    return json.as<bool>();
  }

  int num(const json& json, int default_)
  {
    if (json.is_null()) { return default_; }
    return json.as<int>();
  }

  double real(const json& json, double default_)
  {
    if (json.is_null()) { return default_; }
    return json.as<double>();
  }
}
