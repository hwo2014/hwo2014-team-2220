#include <bootstrap>
#include "controltower"

#include <cars/basecar>

ControlTower::ControlTower(Socket socket,
			   const string& key,
			   BaseCar& car,
			   bool verbose)
  : BaseTower(key, car, verbose)
  , parser(MsgParser(game, verbose))
  , socket(socket)
{
}

void ControlTower::send(const json& msg)
{
  socket.send_requests({msg});
}

json ControlTower::readmsg()
{
  boost::system::error_code error;
  auto msg = socket.receive_response(error);

  if (error) {
    if (error == boost::asio::error::eof) {
      cout << "Connection closed." << endl;
      return null_type();
    }
    throw boost::system::system_error(error);
  }

  return msg;
}

void ControlTower::msg_loop()
{
  json msg = readmsg();

  while (!msg.is_null()) {
    parser.feed_msg(msg);
    if (game.state.need_reply) {
      _car.on_game(game);
      send_reply_by_game();
    }
    msg = readmsg();
  }
}
