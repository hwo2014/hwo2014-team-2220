#include <bootstrap>
#include "basetower"

BaseTower::BaseTower(const string& key, BaseCar& car, bool verbose)
  : _key(key),
    _car(car),
    _verbose(verbose)
{
}

void BaseTower::info(const string& line)
{
  if (_verbose) {
    cout << line << endl;
  }
}

void BaseTower::log(const string& line)
{
  cout << line << endl;
}

void BaseTower::warning(const string& line)
{
  cout << line << endl;
}

void BaseTower::send(const json& msg)
{
  // Really do nothing here.
}

void BaseTower::send_msg(const string& type, const json& data)
{
  json msg;
  msg["msgType"] = type;
  msg["data"] = data;

  info("< " + msg.to_string());
  send(msg);
}

void BaseTower::send_join()
{
  json data;
  data["name"] = _car.name();
  data["key"] = _key;

  send_msg("join", data);
}

void BaseTower::send_dangerous_pong()
{
  // Yes, it's "ping" by definition. See also: techspec.
  send_msg("ping", null_type());
}

void BaseTower::send_msg_with_tick(const string& type, const json& data, int tick)
{
  json msg;
  msg["msgType"] = type;
  msg["data"] = data;
  msg["gameTick"] = tick;

  info("< " + msg.to_string());
  send(msg);
}

void BaseTower::send_throttle(double throttle, int tick)
{
  send_msg_with_tick("throttle", throttle, tick);
}

void BaseTower::send_switch_lane(int dir, int tick)
{
  if (dir == -1) {
    send_msg_with_tick("switchLane", "Left", tick);
    return;
  }
  if (dir == 1) {
    send_msg_with_tick("switchLane", "Right", tick);
    return;
  }

  warning("send_switch_lane(...) with bogus direction.");
  send_dangerous_pong();
}

void BaseTower::send_turbo(const string& message, int tick)
{
  send_msg_with_tick("turbo", message, tick);
}

void BaseTower::send_noop(int tick)
{
  // Yes, it's "ping" by definition. There's nothing like `noop'. See also: techspec.
  send_msg_with_tick("ping", null_type(), tick);
}

void BaseTower::send_reply_by_game()
{
  auto& action = game.myaction();
  if (action._dir != 0) {
    send_switch_lane(action._dir, game.state.tick);
    game.mycar().reserved_dir = action._dir;
    return;
  }
  if (action._turbo) {
    send_turbo(action._turbo_text, game.state.tick);
    return;
  }
  if (action._throttle >= 0.0) {
    send_throttle(action._throttle, game.state.tick);
    return;
  }
  send_noop(game.state.tick);
}

void BaseTower::msg_loop()
{
  // Do nothing here.
}

void BaseTower::run()
{
  send_join();
  msg_loop();
}
